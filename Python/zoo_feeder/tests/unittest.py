import unittest
from zoo_feeder.zoo import Menu


class TestGame(unittest.TestCase):
    def testfeeding(self):
        print("enter meal")
        meal = str.lower(input(""))
        can_be_feeded = False
        if meal in Menu.feed_lst:
            can_be_feeded = True
        else:
            pass
        self.assertEqual(can_be_feeded, True)


if __name__ == '__main__':
    unittest.main()
