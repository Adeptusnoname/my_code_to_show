from zoo_feeder.Exception import *


class Menu():
    Menu_options = ["add_creature", "show_all_creatures", "feed", "Exit"]
    Existed_animal = ["Bear", "Rabbit"]
    animal_class = {}
    feed_lst = ['fish', 'meat', 'carrot', 'grass', 'potato', 'seed']

    def show_menu(self):
        for i, entry in enumerate(self.Menu_options):
            print('{}.{}'.format(i, entry))

        try:
            choice = int(input(""))
        except ValueError:
            raise ChooseException()

        if choice == 0:
            self.add_creature()
        elif choice == 1:
            self.show_all_creatures()
        elif choice == 2:
            self.feed()
        elif choice == 3:
            exit(0)
        else:
            print("Wrong input. Restart the game")
            self.show_menu()

    def add_creature(self):
        creature_lst = []
        print("Which creature from the list should be added")
        for i, entry in enumerate(self.Existed_animal):
            creature_lst.append([i, entry])
            print(i, entry)

        try:
            choice = int(input(""))
        except ValueError:
            raise ChooseException()

        if choice > i:
            print("wrong number")
            self.show_menu()
        elif choice == 0:
            if self.animal_class.get("Bear") == None:
                bear = Bears()
                self.animal_class.update({'Bear': bear})
                self.animal_class.get('Bear').count += 1
                self.animal_class.get('Bear').Hungry = True
            else:
                self.animal_class.get('Bear').count += 1
                self.animal_class.get('Bear').Hungry = True

        elif choice == 1:
            if self.animal_class.get("Rabbit") == None:
                rabbit = Rabbits()
                self.animal_class.update({'Rabbit': rabbit})
                self.animal_class.get('Rabbit').count += 1
                self.animal_class.get('Rabbit').Hungry = True
            else:
                self.animal_class.get('Rabbit').count += 1
                self.animal_class.get('Rabbit').Hungry = True

        self.show_menu()

    def show_all_creatures(self):
        for animal_type in self.animal_class.values():
            print(animal_type)
        self.show_menu()

    def feed(self):
        print("Which animal you will try to feed")
        animals = []
        for i, entry in enumerate(self.animal_class.keys()):
            print(i, entry)
            animals.append(entry)

        try:
            choice = int(input())
        except ValueError:
            raise ChooseException()

        if choice > i:
            print("Wrong number")
            self.show_menu()
        else:
            feeded_animal = animals[choice]

        if len(animals) != 0:
            feeds = []
            print("What you will use for feeding?")
            for i, entry in enumerate(self.feed_lst):
                feeds.append(entry)
                print(i, entry)

            choice = int(input())
            if choice > i:
                print("wrong number")
                self.show_menu()
            else:
                feed_meal = feeds[choice]
                ##feeded_animal feeds
                if feed_meal in self.animal_class.get(feeded_animal).meal:
                    if self.animal_class.get(feeded_animal).Hungry == False:
                        print('This animal have enough food')
                    else:
                        self.animal_class.get(feeded_animal).Hungry = False
                else:
                    print("This animal can't eat this")

                print(self.animal_class.get(feeded_animal))


        else:
            print("Animals not found")

        self.show_menu()


class Animal():
    Hungry = True
    count = 0

    def __init__(self, type='unknown'):
        self.type = type

    def __str__(self):
        string = (
        'Type ' + str(self.type) + " Count " + str(self.count) + '\n' + 'is hungry - ' + str(self.Hungry) + '\n')
        return string


class Bears(Animal):
    agressive = True
    count = 0
    meal = ['fish', 'meat']

    def __init__(self):
        super().__init__(type='Bear')


class Rabbits(Animal):
    agressive = False
    meal = ["carrot"]
    count = 0

    def __init__(self):
        super().__init__(type='Rabbit')


if __name__ == '__main__':
    Game_menu = Menu()
    Game_menu.show_menu()
    exit(0)
