import re
import sqlite3

# def get_spell_text(message):
#     spell_list = get_magic_list_from_bd()
#     eng_spells = get_eng_magic_list_from_bd()
#     text=message.text.replace('/magic ','').capitalize()
#     test_lang = re.search('[a-z]', text)
#     if test_lang == None:
#         if text in spell_list:
#             bot.send_message(message.chat.id, text)
#             bot.send_message(message.chat.id, spell_list[text])
#         else:
#             if len(text)>2:
#                 possible_spells = get_possible_spells(text.lower())
#                 if len(possible_spells)!=0:
#                     bot.send_message(message.chat.id, "Заклинание не найдено. Возможно вы имели в виду одно из следующих:")
#                     for spell in possible_spells:
#                         keyboard = types.InlineKeyboardMarkup()
#                         callback_button = types.InlineKeyboardButton(text="Открыть", callback_data="/magic " + spell.capitalize())
#                         keyboard.add(callback_button)
#                         bot.send_message(message.chat.id, spell, reply_markup=keyboard)
#                 else:
#                     bot.send_message(message.chat.id, "Заклинание не найдено.")
#             else:
#                 bot.send_message(message.chat.id, "Недостаточно символов для поиска заклинания.")
#     else: #eng var
#         if text in eng_spells:
#             bot.send_message(message.chat.id, eng_spells[text][0])
#             bot.send_message(message.chat.id, eng_spells[text][1])
#         else:
#             if len(text) > 2:
#                 possible_spells = get_possible_spells(text.lower())
#                 if len(possible_spells) != 0:
#                     bot.send_message(message.chat.id,
#                                      "Заклинание не найдено. Возможно вы имели в виду одно из следующих:")
#                     for spell in possible_spells:
#                         keyboard = types.InlineKeyboardMarkup()
#                         callback_button = types.InlineKeyboardButton(text="Открыть",
#                                                                      callback_data="/magic " + spell.capitalize())
#                         keyboard.add(callback_button)
#                         bot.send_message(message.chat.id, spell, reply_markup=keyboard)
#                 else:
#                     bot.send_message(message.chat.id, "Заклинание не найдено.")
#             else:
#                 bot.send_message(message.chat.id, "Недостаточно символов для поиска заклинания.")

def get_possible_spells(spell_name):
    possible_spells={}
    test_lang = re.search('[a-z]', spell_name)
    if test_lang == None:
        spells = get_magic_list_from_bd()
        for spell in spells:
            spell_find=spell.lower()
            if spell_find.find(spell_name)!=-1:
                possible_spells[spell]=spells[spell]
    else:
        eng_spells = get_eng_magic_list_from_bd()
        for spell in eng_spells:
            spell_find=spell.lower()
            if spell_find.find(spell_name)!=-1:
                possible_spells[spell]=eng_spells[spell]
    return possible_spells

def get_eng_magic_list_from_bd():
    connect = sqlite3.connect('bot.sqlite')
    cursor = connect.cursor()
    spells = {}
    for row in cursor.execute("SELECT eng,rus,ru_text FROM magic "):
        spells[row[0]] = [row[1], row[2]]
    return spells

def get_magic_list_from_bd():
    connect = sqlite3.connect('bot.sqlite')
    cursor =  connect.cursor()
    spells={}
    for row in cursor.execute("SELECT rus,ru_text FROM magic "):
            spells[row[0]]=row[1]
    return spells
