import requests
import json
import vk, os
from vk import exceptions
from datetime import timedelta
import datetime,time
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.orm import mapper, sessionmaker


class Image(object):
    def __init__(self, name):

        self.name = name

        def __repr__(self):
            return "<Chat('%s')>" % (self.name)


def get_last_post(api,group_id):
    posts = api.wall.get(owner_id="-"+group_id,count=100,filter="postponed",v=3)
    post_count=posts[0]
    posts = api.wall.get(owner_id="-"+group_id,count=5 ,offset=post_count-1,filter="postponed",v=3)

    if posts[0]!=0:
        post_date=0
        if posts[1]['date']>post_date:
            post_date=posts[1]['date']
        date=datetime.datetime.fromtimestamp(post_date).strftime('%Y-%m-%d %H:%M:%S')
        post_date=datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        return post_date


if __name__ == '__main__':
    engine = create_engine('sqlite:///vkbot.sqlite', echo=True, pool_recycle=7200)

    metadata = MetaData()
    images_table = Table('images', metadata,
                       Column('id', Integer, primary_key=True),
                       Column('name', String, unique=True))

    metadata.create_all(engine)
    mapper(Image, images_table)
    Session = sessionmaker(bind=engine)
    bd_session = Session()

    pic_dir = os.curdir + "/picdir/"

    POST_DELAY=timedelta(hours=12)

    login = ''
    password = ''
    app_id = ""
    group_token = ""
    app_token = ""
    group_id = ""

    session = vk.AuthSession(app_id=app_id, user_login=login, user_password=password, scope="wall,photos,docs")
    vk.api.access_token = group_token
    api = vk.API(session)
    # api.wall.post(message="Просто текст...",owner_id="-"+group_id)


    last_post_date=get_last_post(api,group_id) #works only if posts<100
    date=datetime.datetime.now()

    if date.hour>8<20:
        post_date=datetime.datetime(date.year,date.month,date.day,20,0,0)
    else:
        post_date = datetime.datetime(date.year, date.month, date.day, 8, 0, 0)


    if last_post_date>date:
        post_date=last_post_date+POST_DELAY


    pic_files = os.listdir(pic_dir)
    pics = list(filter(lambda x: x.endswith((".gif",".jpg",".png",".jpeg")), pic_files))
    images = bd_session.query(Image.name).all()
    image_list=[]
    for image in images:
        image_list.append(image[0])

    for pic in pics:
        if  pic in image_list:
            print(str(pic) + " was posted before")
        else:
            short_time=str(post_date.year)+'-'+str(post_date.month)+'-'+str(post_date.day)+" "+str(post_date.hour)+":00:00"
            post_unixtime=int(time.mktime(time.strptime(short_time, '%Y-%m-%d %H:%M:%S')))

            # путь к вашему изображению
            pic_path = pic_dir + pic
            img = {'file': (pic_path, open(r'' + pic_path, 'rb'))}

            if pic_path.endswith(".gif"):
                result = api.docs.getWallUploadServer(v=3)
                upload_url = result['upload_url']
                response = requests.post(upload_url,files=img)
                text = json.loads(response.text)
                try:
                    error = text['file']
                except KeyError:
                    print("something is wrong")
                    print(response.text)
                else:
                    result = json.loads(response.text)
                    result = api.docs.save(file=result['file'],v=3)
                    attach='doc10412608_'+str(result[0]['did'])
                    try:
                        api.wall.post(message="", owner_id="-" + group_id, attachments=attach,publish_date=post_unixtime,v=3)
                        post_date = post_date + POST_DELAY
                        new_image = Image(pic)
                        bd_session.add(new_image)
                        bd_session.commit()
                    except exceptions.VkAPIError:
                        print("150 post limit reached")
                        exit(0)

                    time.sleep(1)
            else:
                result = api.photos.getWallUploadServer(v=3)
                upload_url = result['upload_url']

                # # Загружаем изображение на url
                response = requests.post(upload_url, files=img)
                text = json.loads(response.text)
                try:
                    error = text['photo']
                except KeyError:
                    print("something is wrong")
                    print(response.text)
                else:

                    result = json.loads(response.text)

                    # Сохраняем фото на сервере и получаем id
                    try:
                        result = api.photos.saveWallPhoto(hash=result['hash'], photo=result['photo'], server=result['server'],v=3.0)
                        api.wall.post(message="", owner_id="-" + group_id, attachments=result[0]['id'],publish_date=post_unixtime,v=3.0)
                        post_date = post_date + POST_DELAY
                        new_image = Image(pic)
                        bd_session.add(new_image)
                        bd_session.commit()
                    except exceptions.VkAPIError:
                        print("150 post limit reached")
                        exit(0)
                    time.sleep(1)
    bd_session.close()