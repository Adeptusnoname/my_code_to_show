import sqlite3

def get_possible_monsters(monster_name):
    monsters = get_monster_list_from_db()
    possible_monsters = []
    monster_name = monster_name.lower()
    for monster in monsters:
        monster_find = monster.lower()
        if monster_find.find(monster_name) != -1:
            possible_monsters.append(monster.capitalize())
    return possible_monsters


def get_monster_list_from_db():
    connect = sqlite3.connect('bot.sqlite')
    cursor = connect.cursor()
    cursor.execute("SELECT (ru_name) FROM monsters")
    results = cursor.fetchall()
    monsters = []
    for line in results:
        if line != None:
            monsters.append(line[0])
    connect.close()
    return (monsters)


def get_monster_pic_from_db(name):
    connect = sqlite3.connect('bot.sqlite')
    cursor = connect.cursor()
    cursor.execute("SELECT (pic) FROM monsters WHERE ru_name=:monster_name", {"monster_name": name})
    try:
        address = cursor.fetchone()[0]
    except TypeError:
        address = None
    cursor.close()
    return (address)


def get_monster_text_from_db(name):
    connect = sqlite3.connect('bot.sqlite')
    cursor = connect.cursor()
    cursor.execute("SELECT (ru_text) FROM monsters WHERE ru_name=:monster_name", {"monster_name": name})
    try:
        text = cursor.fetchone()[0]
    except TypeError:
        text = ""
    connect.close()
    return (text)



