
def get_monster_list(database):
    files = os.listdir(directory_text)
    files = filter(lambda x: x.endswith('.txt'), files)
    return (files)

def get_magic_list(): # old version
    spells={}
    with open(directory_magic, mode='r') as file:
        spell_name = ""
        spell_text = ""
        for line in file:
            if line.startswith("name!="):
                spells[spell_name] = spell_text
                spell_name = line.replace("name!=","").title().strip('\n').strip(' ')
                spell_text = ""
            else:
                spell_text=spell_text+line
        spells[spell_name] = spell_text
        return(spells)

def add_magic_in_database(database): # used once to translate original book and add data in database
    cursor = database.cursor()
    spell_list=get_magic_list()
    for spell in spell_list:
        if spell!="":
            cursor.execute("SELECT id FROM magic Where rus=:rus_name",{"rus_name": spell.capitalize()})
            data = cursor.fetchall()
            print(data)
            if data!=[]:
                id=(data[0])[0]
                cursor.execute("UPDATE magic set ru_text=:ru_text WHERE id=:id", {"ru_text": spell_list[spell],"id":id})
    database.commit()
    database.close()

def add_monsters_in_bd(database):
    cursor = database.cursor()
    monster_list=get_monster_list()
    for monster in monster_list:
        monster_name = monster.replace(".txt", "").capitalize()
        addres=get_monster_pic(monster_name)
        data=[monster_name,get_monster_text(monster_name),addres]
        cursor.execute("INSERT INTO monsters (ru_name,ru_text,pic) VALUES(?,?,?) " , data)
    database.commit()
    database.close()
