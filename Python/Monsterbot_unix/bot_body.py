import telebot
import os
import random
import sqlite3
import re
import datetime
import time
import requests
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.orm import mapper, sessionmaker
from telebot import types
import spells, monsters
import sys

class statistic:
    USAGE_COUNTER = {"magic": 0, "monster": 0, "random_monster": 0, "class": 0, "unknown": 0, "all_monsters": 0,
                     "all_classes": 0, "start": 0,"dice":0}


# Use this token to access the HTTP API:


TOKEN = ''  # @BotFathers key
bot = telebot.TeleBot(TOKEN)

FIRST_MESSAGE = "Добрый день пользователь. Я бот существующий для предоставления информации по вселенной DnD 5-й редакции и её дополнений. " \
                "Во мне запрограммированы следующие команды: \n" \
                "/dice - бросить кубик. \n" \
                "/random_monster - получить случайного монстра с описанием. \n" \
                "/monster [monster_name] - получить описание введенного монстра. \n" \
                "/magic [имя заклинания] - показывает описание заклинания. \n" \
                "/all_classes - показывает доступные классы. \n" \
                "/class [имя класса] показывает класс. \n " \
                "/feedback [текст] оставить отзыв. \n" \
                "/all_monsters - показывает всех известных мне монстров\n. " \
                "Внимание команды нужно вводить без квадратных скобок. Пример: /magic сфера \n" \
                "Если вы нашли баг либо вам не отвечает сервер, пожалуйста, напишите о нем мне на почту: den032007@mail.ru\n" \
                "либо воспользутейсь функцией feedback. Заранее спасибо."


UNKNOWN_COMMAND = "Неизвестная команда. " \
                  "Во мне запрограммированы следующие команды: \n" \
                  "/dice - бросить кубик. \n" \
                  "/random_monster - получить случайного монстра с описанием. \n" \
                  "/monster [monster_name] - получить описание введенного монстра. \n" \
                  "/all_monsters - показывает всех известных мне монстров. \n" \
                  "/magic [имя заклинания] - показывает описание заклинания. \n" \
                  "/all_classes - показывает доступные классы. \n" \
                  "/class [имя класса] показывает класс. \n" \
                  "/feedback [текст] оставить отзыв. \n" \
                  "Прошу вас использовать одну из них. \n" \
                  "Внимание команды нужно вводить без квадратных скобок. Пример: /magic сфера \n" \
                  "Если вы нашли баг либо вам не отвечает сервер, пожалуйста, напишите о нем мне на почту: den032007@mail.ru\n" \
                  "либо воспользутейсь функцией feedback. Заранее спасибо."

directory_monsters = os.path.dirname(os.path.abspath(__file__)) + '/monsters'
directory_pics = os.path.dirname(os.path.abspath(__file__)) + '/monsters/pics'
directory_magic = os.path.dirname(os.path.abspath(__file__)) + '/magic/magic.txt'


class dirrectories():
    directory_monsters = os.path.dirname(os.path.abspath(__file__)) + '/monsters'
    directory_pics = os.path.dirname(os.path.abspath(__file__)) + '/monsters/pics'


def menu_with_text(message, text):
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button_feedback = types.KeyboardButton(text="Оставить отзыв")
    button_dice = types.KeyboardButton(text="Бросить кубик")
    keyboard.add(button_dice, button_feedback)
    bot.send_message(message.chat.id, text=text, reply_markup=keyboard)


@bot.message_handler(commands=['dice'])
def throw_dice(message):
    stop_wait_feedback(message)
    keyboard = types.InlineKeyboardMarkup()
    callback_button = types.InlineKeyboardButton(text="куб d4",
                                                 callback_data="/dice d4")

    keyboard.add(callback_button)
    callback_button = types.InlineKeyboardButton(text="куб d6",
                                                 callback_data="/dice d6")

    keyboard.add(callback_button)
    callback_button = types.InlineKeyboardButton(text="куб d8",
                                                 callback_data="/dice d8")

    keyboard.add(callback_button)
    callback_button = types.InlineKeyboardButton(text="куб d10",
                                                 callback_data="/dice d10")

    keyboard.add(callback_button)
    callback_button = types.InlineKeyboardButton(text="куб d12",
                                                 callback_data="/dice d12")

    keyboard.add(callback_button)
    callback_button = types.InlineKeyboardButton(text="куб d20",
                                                 callback_data="/dice d20")

    keyboard.add(callback_button)
    callback_button = types.InlineKeyboardButton(text="куб d100",
                                                 callback_data="/dice d100")

    keyboard.add(callback_button)
    bot.send_message(message.chat.id, 'Какой кубик вы хотите бросить?', reply_markup=keyboard)


@bot.message_handler(commands=['start'])
def start(message):
    stop_wait_feedback(message)
    add_chat_id(message)
    statistic.USAGE_COUNTER["start"] = statistic.USAGE_COUNTER["start"] + 1
    update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id) + "\n")
    update_log(str(statistic.USAGE_COUNTER) + "\n")
    menu_with_text(message, FIRST_MESSAGE)


@bot.message_handler(commands=['feedback'])
def leave_feedback(message):
    stop_wait_feedback(message)
    add_chat_id(message)
    save_feedback(message.chat.id, message.text)
    bot.send_message(message.chat.id, "Спасибо за ваш отзыв. ")


# spells!!!!

@bot.message_handler(commands=['Magic'])
def show_magic_big(message):
    show_magic(message)


@bot.message_handler(commands=['magic'])
def show_magic(message, data=""):
    stop_wait_feedback(message)
    add_chat_id(message)
    spell_list = spells.get_magic_list_from_bd()
    eng_spells = spells.get_eng_magic_list_from_bd()
    if data == "":

        text = message.text.lower().replace('/magic ', '').replace("(доп)","").capitalize()
    else:
        text = data.lower().replace('/magic ', '').replace("(доп)","").capitalize().replace('\r', '').replace('\n', '').replace("(доп)","")
    test_lang = re.search('[a-z]', text)
    if test_lang == None:
        if text in spell_list:
            bot.send_message(message.chat.id, text)
            bot.send_message(message.chat.id, spell_list[text])
        else:
            if len(text) > 2:
                possible_spells = spells.get_possible_spells(text.lower())
                if len(possible_spells) != 0:
                    keyboard = types.InlineKeyboardMarkup()
                    for spell in possible_spells:
                        callback_button = types.InlineKeyboardButton(text=spell.capitalize(),
                                                                     callback_data="/magic " + spell.capitalize())
                        keyboard.add(callback_button)
                    bot.send_message(message.chat.id,
                                     "Заклинание не найдено. Возможно вы имели в виду одно из следующих:",
                                     reply_markup=keyboard)
                else:
                    bot.send_message(message.chat.id, "Заклинание не найдено.")
            else:
                bot.send_message(message.chat.id, "Недостаточно символов для поиска заклинания.")
    else:  # eng var
        if text in eng_spells:
            bot.send_message(message.chat.id, eng_spells[text][0])
            bot.send_message(message.chat.id, eng_spells[text][1])
        else:
            if len(text) > 2:
                possible_spells = spells.get_possible_spells(text.lower())
                if len(possible_spells) != 0:
                    keyboard = types.InlineKeyboardMarkup()
                    for spell in possible_spells:
                        callback_button = types.InlineKeyboardButton(text=spell.capitalize(),
                                                                     callback_data="/magic " + spell.capitalize())
                        keyboard.add(callback_button)
                    bot.send_message(message.chat.id,
                                     "Заклинание не найдено. Возможно вы имели в виду одно из следующих:",
                                     reply_markup=keyboard)
                else:
                    bot.send_message(message.chat.id, "Заклинание не найдено.")
            else:
                bot.send_message(message.chat.id, "Недостаточно символов для поиска заклинания.")
    statistic.USAGE_COUNTER["magic"] = statistic.USAGE_COUNTER["magic"] + 1
    update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id) + "\n")
    update_log(str(statistic.USAGE_COUNTER) + "\n")


# spells!!!!



# classes!!!!
@bot.message_handler(commands=['all_classes'])
def show_classes(message):
    stop_wait_feedback(message)
    add_chat_id(message)
    classes = get_all_classes()
    keyboard = types.InlineKeyboardMarkup()
    for game_class in classes:
        callback_button = types.InlineKeyboardButton(text=game_class, callback_data="/class " + game_class)
        keyboard.add(callback_button)
    bot.send_message(message.chat.id, "Доступные классы: ", reply_markup=keyboard)
    statistic.USAGE_COUNTER["all_classes"] = statistic.USAGE_COUNTER["all_classes"] + 1
    update_log("\n" + str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id) + "\n")
    update_log(str(statistic.USAGE_COUNTER))


@bot.message_handler(commands=['Class'])
def show_class_big(message):
    show_class(message)

@bot.message_handler(commands=['class'])
def show_class(message, data=""):
    stop_wait_feedback(message)
    add_chat_id(message)
    if data == "":
        class_name = message.text.lower().replace("/class ", "").capitalize()
    else:
        class_name = data.lower().replace("/class ", "").capitalize()
    class_data = get_class_text_from_db(class_name)


    if len(class_name) > 2:
        if class_data != None:
            text = class_data[0] + "\n" + class_data[1] + "\n\n" + "Кость хитов: " + class_data[
                2] + "\n\n" + "Основная характеристика: " + class_data[3] + "\n\n" + "Спасброски: " + class_data[
                       4] + "\n\n" + "Навыки: " + class_data[5]

            if class_data[7]!=None and class_data[7]!="":
                send_if_pic(class_data[7],message.chat.id)

            if class_data[8] != None and class_data[8] != "":
                send_if_pic(class_data[8], message.chat.id)


            ability_list = get_ability_list(class_name)
            bot.send_message(message.chat.id, text)
            keyboard = types.InlineKeyboardMarkup()
            for ability in ability_list:
                callback_button = types.InlineKeyboardButton(text=ability,
                                                             callback_data="/ability " + ability)

                keyboard.add(callback_button)
            bot.send_message(message.chat.id, "Особенности класса: " + class_name, reply_markup=keyboard)
            archetypes_list = get_archetypes_list(class_name)

            keyboard = types.InlineKeyboardMarkup()
            for archetype in archetypes_list:
                callback_button = types.InlineKeyboardButton(text=archetype,
                                                             callback_data="/arch " + archetype)

                keyboard.add(callback_button)
            bot.send_message(message.chat.id, "Архетипы: " + class_name, reply_markup=keyboard)

            if class_data[6] != None:
                lines = class_data[6].split("\n")
                lines = map(lambda x: x.capitalize(), lines)
                bot.send_message(message.chat.id, "Доступные заклинания:")
                level = ""
                for line in lines:
                    if line.find("Заговоры") and line.find("уровень") == -1:
                        callback_button = types.InlineKeyboardButton(text=line,
                                                                     callback_data="/magic " + line.capitalize())

                        keyboard.add(callback_button)

                    else:
                        if level != "":
                            bot.send_message(message.chat.id, level, reply_markup=keyboard)
                        level = line
                        keyboard = types.InlineKeyboardMarkup()
                bot.send_message(message.chat.id, level, reply_markup=keyboard)
        else:
            classes = get_possible_classes(class_name)
            if len(classes) != 0:
                keyboard = types.InlineKeyboardMarkup()
                for game_class in classes:
                    callback_button = types.InlineKeyboardButton(text=game_class.capitalize(),
                                                                 callback_data="/class " + game_class.capitalize())
                    keyboard.add(callback_button)
                bot.send_message(message.chat.id, "Класс не найден. Возможно вы имели в виду одного из следующих:",
                                 reply_markup=keyboard)

            else:
                bot.send_message(message.chat.id, "Класс не найден")

    else:
        bot.send_message(message.chat.id, "Недостаточно букв для поиска класса")
    statistic.USAGE_COUNTER["class"] = statistic.USAGE_COUNTER["class"] + 1
    update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id) + "\n")
    update_log(str(statistic.USAGE_COUNTER) + "\n")


def show_ability(message, ability_name, class_name):
    ability_list = get_ability_list(class_name)
    ability_text = ability_list[ability_name]
    send_if_pic(ability_text,message.chat.id)
    if len(ability_text) > 4064:
        cut_and_show_message(message, ability_text)
    else:
        bot.send_message(message.chat.id, ability_name + "\n" + ability_text)


def show_archetype_ability(message, ability_name, archetype_name):
    ability_list = get_archetype_ability_list(archetype_name)
    for ability in ability_list.keys():
        if ability.isupper() != True:
            ability_list[ability.upper()]=ability_list[ability]
            ability_list.pop(ability)

    ability_text = ability_list[ability_name.upper()]
    if len(ability_text) > 4064:
        cut_and_show_message(message, ability_text)
    else:
        bot.send_message(message.chat.id, ability_name + "\n" + ability_text)


def get_archetype_ability_list(archetype_name):
    Session = sessionmaker(bind=engine)
    session = Session()
    abylitys = session.query(Archetype_abilitys).filter_by(archetype=archetype_name.upper()).all()
    abylity_list = {}
    for abylity in abylitys:
        abylity_list[abylity.name.strip(" ")] = abylity.description
    session.close()
    return abylity_list


def show_archetype(message, archetype_name, class_name):
    archetype_list = get_archetypes_list(class_name)
    archetype_text = archetype_list[archetype_name][0]

    if archetype_list[archetype_name][1]!=None and archetype_list[archetype_name][1]!="":
        send_if_pic(archetype_list[archetype_name][1], message.chat.id)

    if archetype_list[archetype_name][2]!=None and archetype_list[archetype_name][2]!="":
        send_if_pic(archetype_list[archetype_name][2], message.chat.id)

    ability_list = get_archetype_ability_list(archetype_name)
    keyboard = types.InlineKeyboardMarkup()
    for ability in ability_list:
        callback_button = types.InlineKeyboardButton(text=ability.capitalize(),
                                                     callback_data="/a_ar " + ability.capitalize()) #archetype ability
        keyboard.add(callback_button)
    bot.send_message(message.chat.id, archetype_name + "\n" + archetype_text,reply_markup=keyboard)


def get_ability_list(class_name):
    Session = sessionmaker(bind=engine)
    session = Session()
    game_class = session.query(Class).filter_by(name=class_name).first()
    ability_list = {}
    lines = game_class.abilitys.split("\n")
    ability_text = ""
    for ability in lines:
        if ability.startswith("name!=") == True:
            if ability_text != "":
                ability_list[ability_name] = ability_text
                ability_text = ""
            ability_name = ability.replace("name!=", "").replace("\r", "").strip(" ").capitalize()
        else:
            ability_text = ability_text + ability

    ability_list[ability_name] = ability_text
    session.close()
    return ability_list

def get_archetypes_list(class_name):
    Session = sessionmaker(bind=engine)
    session = Session()
    archetypes = session.query(Archetypes.name,Archetypes.description,Archetypes.pic,Archetypes.magic_table).filter_by(base_class=class_name).all()
    archetype_list={}
    for archetype in archetypes:
        archetype_list[archetype[0].strip(" ").capitalize()] = [archetype[1],archetype[2],archetype[3]]
    session.close()
    return archetype_list



# def get_archetypes_list(class_name):
#     Session = sessionmaker(bind=engine)
#     session = Session()
#     game_class = session.query(Class).filter_by(name=class_name).first()
#     archetypes_list = {}
#     lines = game_class.archetypes.split("\n")
#     archetype_text = ""
#     archetype_name = ""
#     for archetype in lines:
#         if archetype.startswith("name!=") == True:
#             if archetype_text != "":
#                 archetypes_list[archetype_name] = archetype_text
#                 archetype_text = ""
#             archetype_name = archetype.replace("name!=", "").replace("\r", "").strip(" ").capitalize()
#         else:
#             archetype_text = archetype_text + archetype + "\n"
#     archetypes_list[archetype_name] = archetype_text
#     session.close()
#     return archetypes_list

def get_class_text_from_db(class_name):
    database = sqlite3.connect("bot.sqlite")
    cursor = database.cursor()
    cursor.execute(
        "SELECT name,short_description,hit_dice,primary_stat,saves,skills,magic,pic,class_table FROM classes WHERE name=:class_name",
        {"class_name": class_name})
    data = cursor.fetchone()
    return data


def get_all_classes():
    Session = sessionmaker(bind=engine)
    session = Session()
    Classes = session.query(Class.name).all()
    class_list = []
    for game_class in Classes:
        class_list.append(game_class[0])
    session.commit()
    session.close()
    return class_list


def get_possible_classes(class_name):
    class_name = class_name.lower()
    classes = get_all_classes()
    possible_classes = []
    for game_class in classes:
        class_find = game_class.lower()
        if class_find.find(class_name) != -1:
            possible_classes.append(class_find.capitalize())
    return possible_classes


# classes!!!!

# monsters!!!

@bot.message_handler(commands=['all_monsters'])
def all_monsters(message):
    stop_wait_feedback(message)
    add_chat_id(message)
    monster_list = monsters.get_monster_list_from_db()
    keyboard = types.InlineKeyboardMarkup()
    i=0
    for monster in monster_list:
        callback_button = types.InlineKeyboardButton(text=monster, callback_data="/monster " + monster)
        keyboard.add(callback_button)
        i+=1
        if i>=60:
            bot.send_message(message.chat.id, "Монстры: ", reply_markup=keyboard)
            i=0
            keyboard = types.InlineKeyboardMarkup()

    bot.send_message(message.chat.id, "Монстры: ", reply_markup=keyboard)
    statistic.USAGE_COUNTER["all_monsters"] = statistic.USAGE_COUNTER["all_monsters"] + 1
    update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id) + "\n")
    update_log(str(statistic.USAGE_COUNTER) + "\n")


@bot.message_handler(commands=['Monster'])
def show_monster_big(message):
    show_monster(message,"")

@bot.message_handler(commands=['monster'])
def show_monster(message, data=""):
    stop_wait_feedback(message)
    add_chat_id(message)
    if data == "":
        monster_name = message.text.lower().replace("/monster ", "").capitalize()
    else:
        monster_name = data.lower().replace("/monster ", "").capitalize()
    monster_text = monsters.get_monster_text_from_db(monster_name)
    if monster_text != "":
        pic_address = monsters.get_monster_pic_from_db(monster_name)
        if pic_address != None and pic_address!="":
            send_if_pic(pic_address, message.chat.id)
            #bot.send_photo(message.chat.id, pic)

        if len(monster_text) > 4064:
            cut_and_show_message(message, monster_text)
        else:
            bot.send_message(message.chat.id, monster_text)
    else:
        if len(monster_name) > 2:
            possible_monsters = monsters.get_possible_monsters(monster_name.capitalize())
            if len(possible_monsters) != 0:
                bot.send_message(message.chat.id, "Монстр не найден. Возможно вы имели в виду одного из следующих:")
                for monster in possible_monsters:
                    keyboard = types.InlineKeyboardMarkup()
                    callback_button = types.InlineKeyboardButton(text="Открыть",
                                                                 callback_data="/monster " + monster.capitalize())
                    keyboard.add(callback_button)
                    bot.send_message(message.chat.id, monster, reply_markup=keyboard)
            else:
                bot.send_message(message.chat.id, "Монстр не найден.")
        else:
            bot.send_message(message.chat.id, "Недостаточно символов для поиска монстра.")
    statistic.USAGE_COUNTER["monster"] = statistic.USAGE_COUNTER["monster"] + 1
    update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id))
    update_log(str(statistic.USAGE_COUNTER) + "\n")


@bot.message_handler(commands=['random_monster'])
def random_monster(message):
    stop_wait_feedback(message)
    add_chat_id(message)
    monster_list = monsters.get_monster_list_from_db()
    num = random.randint(0, len(monster_list) - 1)
    monster = monster_list[num].capitalize()
    monster_text = monsters.get_monster_text_from_db(monster)
    if monster_text != "":
        pic_address = monsters.get_monster_pic_from_db(monster)
        if pic_address != None:
            send_if_pic(pic_address, message.chat.id)
            #bot.send_photo(message.chat.id, pic)
        if len(monster_text) > 4064:
            cut_and_show_message(message, monster_text)
        else:
            bot.send_message(message.chat.id, monster_text)
    else:
        bot.send_message(message.chat.id, "Монстр не найден. ")
    statistic.USAGE_COUNTER["random_monster"] = statistic.USAGE_COUNTER["random_monster"] + 1
    update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id))
    update_log(str(statistic.USAGE_COUNTER) + "\n")


def add_new_monsters_in_bd(database):
    cursor = database.cursor()
    files = os.listdir(directory_monsters)
    monsters = filter(lambda x: x.endswith('.txt'), files)
    for monster in monsters:
        monster_name = monster.replace(".txt", "").capitalize()

        pic_files = os.listdir(directory_pics)
        pics = {}
        for pic in pic_files:
            num = pic.rindex(".")
            pic_name = pic[0:num].capitalize()
            pic_format = pic[num:]
            pics[pic_name] = pic_format
        try:
            pic_address = dirrectories.directory_pics + "/" + monster_name + pics[monster_name]
        except KeyError:
            pic_address=""
        with open(dirrectories.directory_monsters + "/" + monster_name + '.txt', mode='r', encoding='cp1251') as file:
            monster_text = file.read()

        data = [monster_name, monster_text, pic_address]
        try:
            cursor.execute("INSERT INTO monsters (ru_name,ru_text,pic) VALUES(?,?,?) ", data)
        except sqlite3.IntegrityError:
            pass
    database.commit()
    database.close()


# monsters!!!

@bot.message_handler(content_types=["text"])
def UNKNOWN_COMMAND_menu(message):
    add_chat_id(message)
    if message.text == "Бросить кубик":
        throw_dice(message)
    elif message.text == "Оставить отзыв":
        bot.send_message(message.chat.id, "Пожалуйста, введите сообщение")
        feedback.wait_for_feedback.append(message.chat.id)
    elif message.chat.id in feedback.wait_for_feedback:
        bot.send_message(message.chat.id, text="Спасибо за оставленный отзыв.")
        stop_wait_feedback(message)
        save_feedback(message.chat.id, message.text)
    else:
        menu_with_text(message, UNKNOWN_COMMAND)
        statistic.USAGE_COUNTER["unknown"] = statistic.USAGE_COUNTER["unknown"] + 1
        update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(message.chat.id) + "\n")
        update_log(str(statistic.USAGE_COUNTER) + "\n")


# feedback module

class feedback():
    wait_for_feedback = []


def stop_wait_feedback(message):
    if message.chat.id in feedback.wait_for_feedback:
        feedback.wait_for_feedback.remove(message.chat.id)
    else:
        pass


def save_feedback(chat_id, text):
    with open(os.curdir + "/feedback.txt", mode="a") as file:
        file.write(str(datetime.datetime.now()) + "  " + str(chat_id) + "\n" + text + "\n\n")


# feedback module


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call, *args):
    # Если сообщение из чата с ботом
    if call.message:
        if call.data.startswith("/monster"):
            stop_wait_feedback(call.message)
            show_monster(call.message, call.data)
        elif call.data.startswith("/magic"):
            stop_wait_feedback(call.message)
            show_magic(call.message, call.data)
        elif call.data.startswith("/class"):
            stop_wait_feedback(call.message)
            show_class(call.message, call.data)
        elif call.data.startswith("/ability"):
            stop_wait_feedback(call.message)
            class_name = call.message.text.replace("Особенности класса: ", "")
            show_ability(call.message, call.data.replace("/ability ", "").capitalize(), class_name)
        elif call.data.startswith("/arch"):
            stop_wait_feedback(call.message)
            class_name = call.message.text.replace("Архетипы: ", "")
            show_archetype(call.message, call.data.replace("/arch ", "").capitalize(), class_name)
        elif call.data.startswith("/dice"):
            stop_wait_feedback(call.message)
            dice_max = call.data.replace("/dice d", "")
            result = random.randint(1, int(dice_max))
            bot.send_message(call.message.chat.id, str(result))
            add_chat_id(call.message)
            statistic.USAGE_COUNTER["dice"] = statistic.USAGE_COUNTER["dice"] + 1
            update_log(str(datetime.datetime.now()) + "  chat_id:  " + str(call.message.chat.id) + "\n")
            update_log(str(statistic.USAGE_COUNTER) + "\n")
        elif call.data.startswith("/a_ar"):
            stop_wait_feedback(call.message)
            text_lines=call.message.text.split('\n')
            archetype_name=text_lines[0]
            show_archetype_ability(call.message,call.data.replace("/a_ar ", ""),archetype_name)



    # Если сообщение из инлайн-режима
    elif call.inline_message_id:
        if call.data.startswith("Бросить кубик"):
            pass


# models

class Class(object):
    def __init__(self, name, short_description, hit_dice, primary_stat, saves, skills, magic, abilitys, pic, class_table):
        self.name = name
        self.short_description = short_description
        self.hit_dice = hit_dice
        self.primary_stat = primary_stat
        self.saves = saves
        self.skills = skills
        self.magic = magic
        self.abilitys = abilitys
        self.pic = pic
        self.class_table = class_table


        def __repr__(self):
            return "<Class('%s')>" % (self.name)


class Chat(object):
    def __init__(self, chat_id, last_logon,status):
        self.chat_id = chat_id
        self.last_logon = last_logon
        self.status = status

        def __repr__(self):
            return "<Chat('%s')>" % (self.chat_id)


class Archetypes(object):
    def __init__(self, name, description,pic,magic_table,base_class,eng_name):
        self.name = name
        self.description = description
        self.pic = pic
        self.magic_table = magic_table
        self.base_class = base_class
        self.eng_name = eng_name

        def __repr__(self):
            return "<Archetype('%s')>" % (self.name)


class Archetype_abilitys(object):
    def __init__(self, id, name, description, archetype):
        self.id = id
        self.name = name
        self.description = description
        self.archetype = archetype

        def __repr__(self):
            return "<ability('%s')>" % (self.name)


# system things

def  send_pic(pic_address , chat_id):
    try:
        with open(pic_address, mode='rb') as pic:
            file=pic.read()
            bot.send_photo(chat_id, file)
    except:
        print("cant send" + str(pic))

def send_if_pic(line,chat_id):
    test_line=line.strip(" ")
#test_line='./monsters/pics/test1.jpg'
    if test_line.endswith(".jpg") or test_line.endswith(".jpeg") or test_line.endswith(".png"):
        send_pic(test_line, chat_id)


def update_log(line):
    with open(os.curdir + "/log.txt", mode="a") as file:
        file.write(line)


def add_chat_id(message):
    Session = sessionmaker(bind=engine)
    session = Session()
    Chats = session.query(Chat.chat_id).all()
    id_list = []
    for chat in Chats:
        id_list.append(chat[0], )
    if str(message.chat.id) not in id_list:
        new_chat = Chat(message.chat.id, str(datetime.datetime.now()),"")
        session.add(new_chat)
    else:
        session.query(Chat).filter_by(chat_id=message.chat.id).update({"last_logon": str(datetime.datetime.now())})
    session.commit()
    session.close()

def bot_polling():
    try:
        bot.polling(none_stop=True)
    except:
        time.sleep(30)
        bot_polling()


def send_message_to_users(text):
    Session = sessionmaker(bind=engine)
    session = Session()
    users = session.query(Chat.chat_id,Chat.status).all()
    for user in users:
        if user[1]!="Inactive":
            try:
                bot.send_message(user.chat_id,text=text)
            except:
                change_user_status(user.chat_id)
    session.close()


def change_user_status(id):
    Session = sessionmaker(bind=engine)
    session = Session()
    session.query(Chat).filter_by(chat_id=id).update({"status":"Inactive"})
    # User = session.query(Chat).filter_by(chat_id=int(id)).first()
    # User.status="Inactive"
    session.commit()
    session.close()

def cut_and_show_message(message, text):
    message_size = 4064
    text_list = []
    text_part = "test"
    iterator = 1
    while len(text_part) != 0:
        text_part = text[message_size * (iterator - 1):message_size * iterator]
        iterator += 1
        if len(text_part) > 0:
            text_list.append(text_part)
    for message_text in text_list:
        bot.send_message(message.chat.id, message_text)



if __name__ == '__main__':
    sys.setrecursionlimit(100000)

   #  database_dir = os.path.dirname(os.path.abspath(__file__)) + '/bot.sqlite'

   # engine = create_engine('sqlite:///' + database_dir, echo=True, pool_recycle=7200)

    engine = create_engine('sqlite:///bot.sqlite', echo=True, pool_recycle=7200)

    metadata = MetaData()
    chat_table = Table('chats', metadata,
                       Column('id', Integer, primary_key=True),
                       Column('chat_id', String, unique=True),
                       Column('last_logon', String),
                       Column('status', String))

    archetypes_table = Table('archetypes', metadata,
                       Column('id', Integer, primary_key=True),
                       Column('name', String),
                       Column('description', String),
                       Column('pic', String),
                       Column('magic_table', String),
                       Column('base_class', String),
                       Column('eng_name', String))

    class_table = Table('classes', metadata,
                        Column('id', Integer, primary_key=True),
                        Column('name', String, unique=True),
                        Column('short_description', String),
                        Column('hit_dice', String),
                        Column('primary_stat', String),
                        Column('saves', String),
                        Column('skills', String),
                        Column('magic', String),
                        Column('abilitys', String),
                        Column('pic', String),
                        Column('class_table', String)
                        )

    archetype_ability_table = Table('additional_abilitys', metadata,
                                    Column('id', Integer, primary_key=True),
                                    Column('name', String),
                                    Column('description', String),
                                    Column('archetype', String, unique=True))

    magic_table = Table('magic', metadata,
                        Column('id', Integer, primary_key=True),
                        Column('rus', String, unique=True),
                        Column('eng', String, unique=True),
                        Column('ru_text', String),
                        Column('eng_text', String))

    monster_table = Table('monsters', metadata,
                          Column('id', Integer, primary_key=True),
                          Column('ru_name', String, unique=True),
                          Column('eng_name', String, unique=True),
                          Column('ru_text', String),
                          Column('eng_text', String),
                          Column('pic', String))

    metadata.create_all(engine)
    mapper(Chat, chat_table)
    mapper(Class, class_table)
    mapper(Archetype_abilitys, archetype_ability_table)
    mapper(Archetypes, archetypes_table)

    Session = sessionmaker(bind=engine)
    session = Session()
    session.close()

    conn = sqlite3.connect("bot.sqlite")
    add_new_monsters_in_bd(conn)
    conn.close()

	while True:
		try:
			bot.polling(none_stop=True,interval=0)
		except Exception:
			print('error')
			time.sleep(5)
	
    #get_archetypes_list("Плут")
    #send_message_to_users("Добрый день.")
