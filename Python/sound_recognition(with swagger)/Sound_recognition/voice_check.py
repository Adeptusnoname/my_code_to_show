import os
from flask import render_template, flash, redirect, Flask, request, Response, make_response
from flasgger import Swagger
import soundfile as sf
import json

from pocketsphinx import LiveSpeech, Pocketsphinx, get_model_path, AudioFile, get_data_path, DefaultConfig, Decoder, \
    pocketsphinx, Jsgf, FsgModel
import speech_recognition as sr
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.orm import mapper, sessionmaker

import change_dict

app = Flask(__name__)
Swagger(app)


def pocketsphinx_decode(audio_data, teaching_mode=False, phraze="", language="test_model", keyword_entries=None,
                        grammar=None,
                        show_all=True):
    if isinstance(language, str):  # directory containing language data
        language_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), language)
        acoustic_parameters_directory = os.path.join(language_directory, "acoustic-model")
        language_model_file = os.path.join(language_directory, "language-model.lm.bin")
        phoneme_dictionary_file = os.path.join(language_directory, "pronounciation-dictionary.dict")

    # create decoder object
    config = pocketsphinx.Decoder.default_config()
    config.set_string("-hmm",
                      acoustic_parameters_directory)  # set the path of the hidden Markov model (HMM) parameter files
    config.set_string("-lm", language_model_file)
    config.set_string("-dict", phoneme_dictionary_file)
    config.set_string("-logfn", os.devnull)  # disable logging (logging causes unwanted output in terminal)

    decoder = pocketsphinx.Decoder(config)

    # obtain audio data
    raw_data = audio_data.get_wav_data(convert_rate=16000,
                                       convert_width=2)  # the included language models require audio to be 16-bit mono 16 kHz in little-endian format
    if keyword_entries is not None:  # explicitly specified set of keywords
        with open("keyfile", mode='w') as f:
            # generate a keywords file - Sphinx documentation recommendeds sensitivities between 1e-50 and 1e-5
            for keyword in keyword_entries:
                sens = int(100 * keyword[1]) - 110
                if sens > 0: sens = str("+" + str(sens))
                f.writelines(
                    "{} /1e{}/\n".format(keyword[0], str(sens)))

            f.flush()

            # perform the speech recognition with the keywords file (this is inside the context manager so the file isn;t deleted until we're done)
            decoder.set_kws("keywords", f.name)
            decoder.set_search("keywords")
            decoder.start_utt()  # begin utterance processing
            decoder.process_raw(raw_data, False,
                                True)  # process audio data with recognition enabled (no_search = False), as a full utterance (full_utt = True)
            decoder.end_utt()  # stop utterance processing

            # f.close()
    else:
        decoder.start_utt()  # begin utterance processing

        decoder.process_raw(raw_data, False,
                            True)  # process audio data with recognition enabled (no_search = False), as a full utterance (full_utt = True)
        decoder.end_utt()  # stop utterance processing

    hypothesis = decoder.hyp()
    total__score = False
    flag = 0
    if hypothesis is not None and teaching_mode == False and score.score == []:
        print(hypothesis.hypstr)

        if phraze != "":
            Session = sessionmaker(bind=engine)
            session = Session()
            if hypothesis.hypstr == phraze:

                session.query(Phraze).filter(Phraze.phraze == phraze).update({'probability': "1"})
                session.commit()

                session.close()

                create_markers(phraze)
            else:
                session.query(Phraze).filter(Phraze.phraze == phraze).update({'probability': "0.5"})
                session.commit()

                session.close()

        else:
            phraze = guess_what_user_said(hypothesis.hypstr)
            if phraze != "":
                result, length, position_result, negative_score = check_phraze(hypothesis.hypstr, phraze)

                percentage = (100 / length)
                result_percents = percentage * result

                data = ""
                # print("Score result: {} from: {} . Correct on {} %".format(result, length, result_percents))
                data += "Score result: {} from: {} . Correct on {} %".format(result, length, result_percents) + " "

                if result_percents >= 60:
                    # print("Passed!")
                    data += "Passed!" + "\n"
                    total__score = True
                else:
                    # print("Failed")
                    data += "Failed" + "\n"
                    flag += 1

                if negative_score > 0:
                    # print("Negative words result: {}".format(negative_score))
                    data += "Negative words result: {}".format(negative_score) + " "
                    # print("Failed!")
                    data += "Failed!" + "\n"
                    flag += 1
                else:
                    data += "Negative words result: {}".format(negative_score) + " "
                    data += "Passed!" + "\n"

                # print("Position result: {}".format(position_result))
                data += "Position result: {}".format(position_result) + " "
                if length > 3:
                    if result >= -1:
                        # print("Passed!")
                        data += "Passed!" + "\n"
                        total__score = True
                    else:
                        # print("Failed!")
                        data += "Failed!" + "\n"
                        flag += 1
                else:
                    if result >= 0:
                        # print("Passed!")
                        data += "Passed!" + "\n"
                        total__score = True
                    else:
                        # print("Failed!")
                        data += "Failed!" + "\n"
                        flag += 1

                if flag > 0:
                    total__score = False

                if total__score:
                    # print("Total score: Passed")
                    data += "Total score: Passed" + "\n"
                else:
                    # print("Total score: Failed")
                    data += "Total score: Failed" + "\n"

                # print("User said: {}".format(phraze))
                data += "{}".format(phraze) + "\n"
                return data
            else:
                # print("phraze not recognized")
                data = "phraze not recognized"
                return data

    elif hypothesis is not None and teaching_mode == False and score.score != []:
        # print("go here")
        # заплатка

        add_text = []
        text_with_score = {}
        phraze_score = 0

        base_text = phraze.split(" ")
        for one_phraze in score.score:
            phraze_score = 0
            add_text = []
            add_end_text = []
            if len(one_phraze) == len(base_text):
                for i in range(len(base_text)):
                    if base_text[i] == one_phraze[i]:
                        add_text.append(base_text[i])
                        phraze_score += 1
                    else:
                        break
                for i in range(len(base_text) - phraze_score):
                    if base_text[-i - 1] == one_phraze[-i - 1]:
                        add_end_text.append(base_text[-i - 1])
                        phraze_score += 1
                    else:
                        break
                add_end_text.reverse()
                if len(add_end_text) <= 0:
                    text_with_score[phraze_score] = ' '.join(add_text)
                else:
                    end_text = ' '.join(add_end_text)
                    text_with_score[phraze_score] = ' '.join(add_text)
                    text_with_score[phraze_score] += " " + end_text

            else:
                for i in range(len(base_text)):
                    try:
                        if base_text[i] == one_phraze[i]:
                            add_text.append(base_text[i])
                            phraze_score += 1
                        else:
                            break
                    except(IndexError):
                        pass

                for i in range(len(base_text) - phraze_score):
                    try:
                        if base_text[-i - 1] == one_phraze[-i - 1]:
                            add_end_text.append(base_text[-i - 1])
                            phraze_score += 1
                        else:
                            break
                    except(IndexError):
                        pass

                add_end_text.reverse()
                if len(add_end_text) <= 0:
                    text_with_score[phraze_score] = ' '.join(add_text)
                else:
                    end_text = ' '.join(add_end_text)
                    text_with_score[phraze_score] = ' '.join(add_text)
                    text_with_score[phraze_score] += " " + end_text

        new_score = 0
        for text in text_with_score:
            if text > new_score:
                new_score = text

        cutted_phraze = text_with_score[new_score]

        # lets, find, bad words
        create_markers(phraze, cutted_phraze)
        score.score = []

    elif teaching_mode == True:  # check
        result = self_teaching(hypothesis.hypstr, language_directory, phraze, audio_data)
        if result != None:
            print(result)
        else:
            pass


    else:
        print("Phaze cannot be recognized")


def create_markers(phraze, not_full_phraze=""):
    Session = sessionmaker(bind=engine)
    session = Session()

    if not_full_phraze == "":
        text = phraze.split(" ")
    else:
        text = not_full_phraze.split(" ")

    first_words = text[0::2]
    second_words = text[1::2]

    i = 0
    markers = ""
    for i in range(len(second_words)):
        markers += str(first_words[i] + " " + second_words[i] + ",")

    first_words2 = text[1::2]
    second_words2 = text[2::2]

    markers2 = ""
    for i in range(len(second_words2)):
        markers2 += str(first_words2[i] + " " + second_words2[i] + ",")

    session.query(Phraze).filter(Phraze.phraze == phraze).update({'positive_markers': str(markers + "|2|" + markers2)})
    session.commit()

    session.close()
    #
    # create_markers(phraze)


def guess_what_user_said(decoded_phraze):
    decoded_words = decoded_phraze.split(" ")
    guessed_phraze=""
    Session = sessionmaker(bind=engine)
    session = Session()

    phrazes = session.query(Phraze.phraze).all()

    score = {}
    points = 0
    int_score = {}
    negative_markers = []

    negative_markers_from_db = session.query(Negative_key.key).all()
    for marker in negative_markers_from_db:
        text = str(marker).replace("'", "").replace("(", "").replace(")", "").replace(",", "")
        negative_markers.append(text)

    marker_flag = False
    markers_to_check = []

    for marker in negative_markers:
        if decoded_phraze.find(marker) != -1:
            marker_flag = True  # marker also should exist in tested_phrazes
            markers_to_check.append(marker)

    for phraze in phrazes:
        phraze_flag = False
        text = str(phraze).replace("'", "").replace("(", "").replace(")", "").replace(",", "")
        for marker in negative_markers:
            if text.find(marker) != -1:
                phraze_flag = True

        if marker_flag == phraze_flag:
            result, length, position_result, negative_score = check_phraze(decoded_phraze, text)
            int_score[phraze] = [result, length, position_result, negative_score]

            best_score = 0
        for phraze in int_score:
            if int_score[phraze][0] > best_score:
                best_score = int_score[phraze][0]
                guessed_phraze = str(phraze).replace("'", "").replace("(", "").replace(")", "").replace(",", "")

    points = 0

    phraze_words = guessed_phraze.split(" ")
    for word in decoded_words:
        if word in phraze_words:
            points += 1
            phraze_words.remove(word)
        else:
            points -= 1

    if points < 0:
        guessed_phraze = ""

    session.close()

    return (guessed_phraze)


def check_phraze(decoded_phraze, phraze):
    test_phraze = phraze

    Session = sessionmaker(bind=engine)
    session = Session()

    phraze = session.query(Phraze).filter(Phraze.phraze == phraze).first()

    positive_markers = phraze.positive_markers.split("|2|")
    positive_markers1 = positive_markers[0].rstrip(",").split(",")
    positive_markers2 = positive_markers[1].rstrip(",").split(",")
    negative_markers = []

    negative_markers_from_db = session.query(Negative_key.key).all()
    for marker in negative_markers_from_db:
        text = str(marker).replace("'", "").replace("(", "").replace(")", "").replace(",", "")
        negative_markers.append(text)

    best_score = 0
    score = 0
    position_score = 0
    negative_score = 0

    # score 1
    original_positions = {}
    for marker in positive_markers1:
        original_positions[marker] = test_phraze.find(marker)

    original_position_in_lines = {}
    for val in original_positions.items():
        i = 0
        for val2 in original_positions.values():
            if val[1] > val2:
                i += 1
                original_position_in_lines[val[0]] = i
        if i == 0:
            original_position_in_lines[val[0]] = i

    positive_position_in_lines = {}
    positive_markers_position = {}
    for marker in positive_markers1:
        if decoded_phraze.find(marker) != -1:
            positive_markers_position[marker] = decoded_phraze.find(marker)
            score = score + 1

    for val in positive_markers_position.items():
        i = 0
        for val2 in positive_markers_position.values():
            if val[1] > val2:
                i += 1
                positive_position_in_lines[val[0]] = i
        if i == 0:
            positive_position_in_lines[val[0]] = i

    for marker in positive_position_in_lines.items():
        if original_position_in_lines[marker[0]] < positive_position_in_lines[marker[0]]:
            position_score -= 1

    # score 1

    position_score1 = position_score
    best_score = score
    length = len(positive_markers1)

    # score 2

    score = 0
    position_score = 0

    # score 1
    original_positions = {}
    for marker in positive_markers2:
        original_positions[marker] = test_phraze.find(marker)

    original_position_in_lines = {}
    for val in original_positions.items():
        i = 0
        for val2 in original_positions.values():
            if val[1] > val2:
                i += 1
                original_position_in_lines[val[0]] = i
        if i == 0:
            original_position_in_lines[val[0]] = i

    positive_position_in_lines = {}
    positive_markers_position = {}
    for marker in positive_markers2:
        if decoded_phraze.find(marker) != -1:
            positive_markers_position[marker] = decoded_phraze.find(marker)
            score = score + 1

    for val in positive_markers_position.items():
        i = 0
        for val2 in positive_markers_position.values():
            if val[1] > val2:
                i += 1
                positive_position_in_lines[val[0]] = i
        if i == 0:
            positive_position_in_lines[val[0]] = i

    for marker in positive_position_in_lines.items():
        if original_position_in_lines[marker[0]] < positive_position_in_lines[marker[0]]:
            position_score -= 1

    # score 2

    # negative
    for marker in negative_markers:
        if decoded_phraze.find(marker) != -1:
            negative_score += 1
    # negative


    if score >= best_score and position_score >= position_score1:
        length = len(positive_markers2)
    elif score >= best_score and position_score < position_score1:
        length = len(positive_markers2)
        position_score = position_score1
    elif score < best_score and position_score >= position_score1:
        length = len(positive_markers1)
        score = best_score
        position_score = position_score
    elif score < best_score and position_score < position_score1:
        length = len(positive_markers1)
        score = best_score
        position_score = position_score1

    return score, length, position_score, negative_score


def listen_from_file(file, teaching_mode=False, phraze=""):
    data = ""
    r = sr.Recognizer()
    r.energy_threshold = 8000
    r.dynamic_energy_threshold = True
    r.phrase_threshold = "1e-100"
    #file="temp_files/file1.ogg"
    audio_file = (file)
    #(ogg files should be converted to wav)
    try:
        with sr.AudioFile(audio_file) as source:
            audio = r.record(source)
    except(ValueError):
        # data, samplerate = sf.read(file)
        # file = file[0:-4]
        # sf.write(file+"test.wav", data, samplerate)
        data = "Wrong file format."



    if data != "Wrong file format.":
        if teaching_mode == False and phraze == "":
            data = pocketsphinx_decode(audio, language="teach_model")  # dont forget to change
        elif teaching_mode == False and phraze != "":
            pocketsphinx_decode(audio, False, phraze, language="teach_model")  # Check_new_model_and_create_score
        elif teaching_mode == True:
            pocketsphinx_decode(audio, teaching_mode, phraze,
                                language="teach_model")  # dont forget to change to test_model

    if data != "":
        return data


def create_vocabluary(language_dir):
    dict_trigram1 = []
    dict_trigram2 = []
    dict_trigram3 = []
    trigram = 0
    with open(os.path.join(language_dir, "language-model.lm.bin"), mode='r', encoding='utf-8') as file:
        for text_line in file.readlines():
            if text_line.startswith("\\1-grams:"):
                trigram = 1
            elif text_line.startswith("\\2-grams:"):
                trigram = 2
            elif text_line.startswith("\\3-grams:"):
                trigram = 3
            else:
                if text_line.startswith("\\data") == False and text_line.startswith(
                        "ngram") == False and text_line.startswith("\\end") == False and text_line != "\n":
                    text_line = text_line.rstrip("\n")
                    text = text_line.split(" ")
                    if trigram == 1:
                        dict_trigram1.append([text[0], text[1:-1], text[-1]])
                    elif trigram == 2:
                        dict_trigram2.append([text[0], text[1:-1], text[-1]])
                    elif trigram == 3:
                        dict_trigram3.append([text[0], text[1:]])

    file.close()

    return dict_trigram1, dict_trigram2, dict_trigram3


def find_word_in_trigram(word, trigram):
    result = False
    for line in trigram:
        if word == line[1]:
            result = True
    return result


def check_word_in_dict(word, language_dir="teach_model"):
    language_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), language_dir)
    acoustic_parameters_directory = os.path.join(language_directory, "acoustic-model")
    language_model_file = os.path.join(language_directory, "language-model.lm.bin")
    phoneme_dictionary_file = os.path.join(language_directory, "pronounciation-dictionary.dict")

    # create decoder object


    config = pocketsphinx.Decoder.default_config()
    config.set_string("-hmm",
                      acoustic_parameters_directory)  # set the path of the hidden Markov model (HMM) parameter files
    config.set_string("-lm", language_model_file)
    config.set_string("-dict", phoneme_dictionary_file)
    config.set_string("-logfn", os.devnull)  # disable logging (logging causes unwanted output in terminal)

    decoder = pocketsphinx.Decoder(config)

    if decoder.lookup_word(word) == None:
        change_dict.add_word_transcription(word)
        return True  # now_it_exists
    else:
        return False  # existed before


def self_teaching(decoded_phraze, language_dir, teaching_phraze, audio_data):
    trigram1, trigram2, trigram3 = create_vocabluary(language_dir)

    decoded_text = decoded_phraze.split(" ")
    base_text = teaching_phraze.split(" ")
    i = 0

    if decoded_text != base_text:

        print("iteration")

        score.score.append(decoded_text)
        if len(score.score) < 10:

            if len(base_text) <= 3:
                if len(base_text) == 3:
                    if find_word_in_trigram(base_text, trigram3) != True:
                        trigram3.append([-1.00, base_text, "-0.00", "t"])

                if len(base_text) == 2:
                    if find_word_in_trigram(base_text, trigram2) != True:
                        trigram2.append([-1.50, base_text, "-0.00", "t"])

            else:
                triwords = []
                twowords = []
                for i in range(len(base_text) - 2):
                    triwords.append([base_text[i], base_text[i + 1], base_text[i + 2]])

                for i in range(len(base_text) - 1):
                    twowords.append([base_text[i], base_text[i + 1]])

                for words in triwords:
                    if find_word_in_trigram(words, trigram3) != True:
                        trigram3.append([-0.50, words, "-0.00", "t"])

                for words in twowords:
                    if find_word_in_trigram(words, trigram2) != True:
                        trigram2.append([-1.00, words, "-0.00", "t"])

            i = 0
            for word in base_text:
                try:
                    if word != decoded_text[i]:
                        score.words_to_change.append(decoded_text[i])
                        if check_word_in_dict(word, language_dir) != True:
                            if find_word_in_trigram([word], trigram1) != True:
                                if len(word) <= 2:
                                    trigram1.append([-6, [word], "-0.00", "t"])
                                elif len(word) > 2:
                                    trigram1.append([-2.50, [word], "-0.00", "t"])
                            else:
                                trigram1 = change_dict.increase_word(word, trigram1)
                                trigram1 = change_dict.decrease_word(decoded_text[i], trigram1)
                        i += 1
                    else:
                        i += 1

                except(IndexError):
                    if check_word_in_dict(word) != True:
                        if find_word_in_trigram([word], trigram1) != True:
                            if len(word) <= 2:
                                trigram1.append([-6, [word], "-0.00", "t"])
                            elif len(word) > 2:
                                trigram1.append([-2.50, [word], "-0.00", "t"])
                            break
                        else:
                            trigram1 = change_dict.increase_word(word, trigram1)
                            break

            change_dict.rewrite_dict(trigram1, trigram2, trigram3, language_dir);

            pocketsphinx_decode(audio_data, True, teaching_phraze, language=language_dir)

            return None
        else:
            pocketsphinx_decode(audio_data, False, teaching_phraze, language=language_dir)

    else:
        score.score = []
        score.words_to_change = []

        create_markers(teaching_phraze)
        return "Phraze confirmed"


def recheck_dict():
    Session = sessionmaker(bind=engine)
    session = Session()

    phrazes = session.query(Phraze).all()
    for phraze in phrazes:
        if phraze.positive_markers == None or phraze.positive_markers == "":
            phraze_to_check = phraze.phraze
            file = phraze.audio_for_check
            ext = file.rsplit('.', 1)[-1]
            if (ext == "wav" or ext == "raw"):
                listen_from_file(file, True, phraze_to_check)
            else:
                print("File " + file + "have wrong format")

    session.close()


def check_new_phrazes():
    Session = sessionmaker(bind=engine)
    session = Session()

    check_flag = False  # nothing to change
    phrazes = session.query(Phraze).all()
    for phraze in phrazes:
        if phraze.positive_markers == None or phraze.positive_markers == "":
            check_flag = True
            recheck_dict()

    session.close()

    if check_flag == True:
        for phraze in phrazes:
            listen_from_file(phraze.audio_for_check, False, phraze.phraze)


class Phraze(object):
    def __init__(self, id, phraze, sound, markers, probability):
        self.id = id
        self.phraze = phraze
        self.sound = sound
        self.markers = markers
        self.probability = probability


class Negative_key(object):
    def __init__(self, id, key):
        self.id = id
        self.key = key


class Score(object):
    score = []
    words_to_change = []


@app.route("/file", methods=['POST'])
def file():
    """
                This is the sound recognition API
                ---
                tags:
                  - Sound recognition
                consumes:
                  - multipart/form-data
                parameters:
                  - in: formData
                    name: file
                    type: file
                    description: "file to upload"
                    required: true
                    description: Upload your file in wav format.
                responses:
                  500:
                    description: Unknown file format.
                  200:
                    description: File recognized. See text in results
                    schema:

                """
    files = request.files

    # file=request.form['files']
    # for test in file:
    #     print(test)

    for file in files:
        temp_file = files[file]
        data = temp_file.read()

        name_flag = 0
        i = 0
        while name_flag == 0:
            if os.path.exists("temp_files/file" + str(i) + ".wav"):
                i = i + 1
            else:
                name_flag = 1
                with open("temp_files/file" + str(i) + ".wav", mode="wb") as new:
                    new.write(data)


    result = listen_from_file("temp_files/file" + str(i) + ".wav", False, "")
    os.remove("temp_files/file" + str(i) + ".wav")


    if result=="Wrong file format." or  result=="phraze not recognized":
        reponse_json = json.dumps({'message': result})
        return Response(response=reponse_json, status=500, mimetype='applicatio/json')
        # return result, 500, {'ContentType': 'text/html', 'Access-Control-Allow-Origin': '*'}
    else:
        data=result.split("\n")
        score_result=data[0]
        negative_result = data[1]
        postition_result=data[2]
        total_score=data[3]
        user_said=str(data[4])
        reponse_json=json.dumps({'score result': score_result,
                    'negative_score': negative_result,
                    'position_score': postition_result,
                    'total_score':total_score,
                    'user_said':user_said}, ensure_ascii=False)
        # return result, 200, {'ContentType': 'text/html', 'Access-Control-Allow-Origin': '*'}
        return Response(response=reponse_json, status=200, mimetype='applicatio/json')


if __name__ == '__main__':
    score = Score

    phraze = ""

    engine = create_engine('sqlite:///database.sqlite', echo=True, pool_recycle=7200)

    metadata = MetaData()
    Phraze_table = Table('phrazes', metadata,
                         Column('id', Integer, primary_key=True),
                         Column('phraze', String, unique=True),
                         Column('audio_for_check', String, default=""),
                         Column('positive_markers', String, default=""),
                         Column('probability', String, default=""))

    Negative_key_table = Table('negative keys', metadata,
                               Column('id', Integer, primary_key=True),
                               Column('key', String, unique=True))

    metadata.create_all(engine)

    mapper(Phraze, Phraze_table)
    mapper(Negative_key, Negative_key_table)

    Session = sessionmaker(bind=engine)
    session = Session()
    session.close()

    check_new_phrazes()

    app.run(host='127.0.0.1', port=4567)

    #
    # if len(sys.argv) > 0:  # DONT FORGET!!!
    #     # file = sys.argv[1]
    #
    #     # file = 'sound_tests/decoder-test.wav'
    #     # file = 'sound_tests/12345.wav'
    #     # file = 'sound_tests/english.wav'
    #     # file = 'sound_tests/rus_john.wav'
    #     # file='sound_tests/goforward.raw'
    #     # file='sound_tests/travelling.raw'
    #     # file ='sound_tests/thunder_in_may.wav'
    #     # file = 'sound_tests/data.wav'
    #     # file = 'sound_tests/credit3(positive).wav'
    #     # file = 'sound_tests/credit2(positive).wav'
    #     # file = 'sound_tests/credit(positive).wav'
    #     # file = 'sound_tests/credit2(negative).wav'
    #     # file = 'sound_tests/credit(negative).wav'
    #     # file = 'sound_tests/cadabra(negative).wav'
    #
    #     # teaching_module
    #     # ok
    #     # file = 'sound_tests/teaching/good_day.wav'
    #     # phraze="добрый день"
    #
    #     # file = 'sound_tests/teaching/clientbank_online_negative.wav'
    #
    #     # ok
    #     # file = 'sound_tests/teaching/not_confirm_doc.wav'
    #     # phraze="я не согласен с условиями договора
    #
    #     # ok
    #     # file = 'sound_tests/teaching/my_name.wav'
    #     # phraze = "меня зовут"
    #
    #     # ok
    #     # file = 'sound_tests/teaching/personal_data.wav'
    #     # phraze = "персональных данных"
    #
    #     # ok
    #     # file = 'sound_tests/teaching/update_my_cell.wav'
    #     # phraze = "добрый день я пришёл обновить свой счёт"
    #
    #     # ok
    #     # file = 'sound_tests/teaching/give_my.wav'
    #     # phraze = "даю своё согласие"
    #
    #     # ok
    #     # file = 'sound_tests/teaching/confirm_doc.wav'
    #     # phraze = "обязуюсь соблюдать условия договора"
    #
    #     # phraze = "прошу подписать меня на рассылку"
    #     #file = 'sound_tests/teaching/subscribe_me.wav'
    #
    #     # ok
    #     #file = 'sound_tests/teaching/agree_with_paper.wav'
    #     # phraze = "согласен с условиями договора"
    #
    #     # ok
    #     # file = 'sound_tests/teaching/give_me_credit.wav'
    #     # phraze = "выдать мне кредит"
    #
    #     # teaching_module
    #
    #     # show
    #     # file = 'sound_tests/teaching/test_to_show/client_bank(2).wav'
    #     # file = 'sound_tests/teaching/test_to_show/clientbank_online.wav'
    #     # file = 'sound_tests/teaching/test_to_show/clientbank_online_negative.wav'
    #     # file = 'sound_tests/teaching/test_to_show/internet_banking(neg).wav'
    #     # file = 'sound_tests/teaching/test_to_show/internet_banking(pos).wav'
    #     # file = 'sound_tests/teaching/test_to_show/alfa-click(neg).wav'
    #     # file = 'sound_tests/teaching/test_to_show/alfa-click(pos).wav'
    #     # file = 'sound_tests/teaching/test_to_show/agree_with_paper.wav'
    #     # file = 'sound_tests/teaching/test_to_show/subscribe_me.wav'
    #     # file = 'sound_tests/teaching/test_to_show/give_me_credit.wav'
    #     # file = 'sound_tests/teaching/test_to_show/not_confirm_doc.wav'
    #     # file = 'sound_tests/teaching/test_to_show/confirm_doc.wav'
    #     # show
    #
    #
    #
    #     if os.path.exists(file):
    #         ext = file.rsplit('.', 1)[-1]
    #
    #         if (ext == "wav" or ext == "raw"):
    #
    #             data_path = get_data_path()
    #
    #             print("start recognition")
    #             listen_from_file(file, False, phraze)  # - second argument - teaching mode
    #
    #         else:
    #             print("wrong file format")
    #     else:
    #         print("file not found")
    # else:
    #     print("not enough arguments")
