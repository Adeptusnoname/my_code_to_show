import voice_check
import io,os

def increase_word(word, trigram1):
    for line in trigram1:
        if word == line[1][0]:
            if float(line[0])<10:
                chance = float(line[0]) + 0.5
                add_info = line[2]
                trigram1.remove(line)
                trigram1.append([str(chance), [word], add_info])
            # add word in dict
    return trigram1


def decrease_word(word, trigram1):
    for line in trigram1:
        if word == line[1][0]:
            if float(line[0]) > -20:
                chance = float(line[0]) - 0.5
                word = word  # just to describe what i do
                add_info = line[2]
                trigram1.remove(line)
                trigram1.append([str(chance), [word], add_info])
            break

    return trigram1


def add_word_transcription(word):
    flag=False
    with io.open(os.curdir + '/Rus/pronounciation-dictionary.dict', encoding='utf-8') as file:
        for line in file:
            if word in line:
                test = line.split(" ")
                if test[0] == word:
                    flag=True
                    with open(os.curdir + '/teach_model/pronounciation-dictionary.dict', mode='a',
                              encoding='utf-8') as file2:
                        file2.write(line)
                    file2.close()
                    break
        file.close()
        if flag==False:
            print("Word not found in dictinary")
            exit(0)


def rewrite_dict(trigram1, trigram2, trigram3, dir):
    with open(os.path.join(os.curdir, "teach_model\\language-model.lm.bin"), mode='w', encoding='utf-8') as file:
        file.write('\data\\' + '\n')
        file.write('ngram 1=' + str(len(trigram1)) + '\n')
        file.write('ngram 2=' + str(len(trigram2)) + '\n')
        file.write('ngram 3=' + str(len(trigram3)) + '\n')
        file.write('\n')
        file.write('\\1-grams:' + '\n')

        for line in trigram1:
            file.write(str(line[0]) + " " + (' '.join(line[1])) + " " + line[2] + '\n')
        file.write('\n')
        file.write('\\2-grams:' + '\n')

        for line in trigram2:
            data = ' '.join(line[1])
            file.write(str(line[0]) + " " + data + " " + str(line[2]) + '\n')
        file.write('\n')
        file.write('\\3-grams:' + '\n')

        for line in trigram3:
            file.write(str(line[0]) + " " + (' '.join(line[1])) + '\n')
        file.write('\n')
        file.write('\n')
        file.write('\end\\')

    file.close()