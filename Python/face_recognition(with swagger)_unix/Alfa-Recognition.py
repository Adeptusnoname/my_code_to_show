
# coding: utf-8

# In[ ]:

from flask import Flask, request, render_template, Response
import jsonpickle
from werkzeug.utils import secure_filename
import os, pathlib
import pickle
import cv2
import openface
import numpy as np
import dlib
from PIL import Image
from flasgger import Swagger

app = Flask(__name__)
Swagger(app)

def getRep(rgbImg,face):
    bb=face[0][0]
    shape=face[1][0]
    alignedFace=align(96, rgbImg, bb, shape)
    if alignedFace is None:
        return None
    rep=net.forward(alignedFace)
    return rep

def align(imgDim, rgbImg, bb, landmarks):
    TEMPLATE = np.float32([
    (0.0792396913815, 0.339223741112), (0.0829219487236, 0.456955367943),
    (0.0967927109165, 0.575648016728), (0.122141515615, 0.691921601066),
    (0.168687863544, 0.800341263616), (0.239789390707, 0.895732504778),
    (0.325662452515, 0.977068762493), (0.422318282013, 1.04329000149),
    (0.531777802068, 1.06080371126), (0.641296298053, 1.03981924107),
    (0.738105872266, 0.972268833998), (0.824444363295, 0.889624082279),
    (0.894792677532, 0.792494155836), (0.939395486253, 0.681546643421),
    (0.96111933829, 0.562238253072), (0.970579841181, 0.441758925744),
    (0.971193274221, 0.322118743967), (0.163846223133, 0.249151738053),
    (0.21780354657, 0.204255863861), (0.291299351124, 0.192367318323),
    (0.367460241458, 0.203582210627), (0.4392945113, 0.233135599851),
    (0.586445962425, 0.228141644834), (0.660152671635, 0.195923841854),
    (0.737466449096, 0.182360984545), (0.813236546239, 0.192828009114),
    (0.8707571886, 0.235293377042), (0.51534533827, 0.31863546193),
    (0.516221448289, 0.396200446263), (0.517118861835, 0.473797687758),
    (0.51816430343, 0.553157797772), (0.433701156035, 0.604054457668),
    (0.475501237769, 0.62076344024), (0.520712933176, 0.634268222208),
    (0.565874114041, 0.618796581487), (0.607054002672, 0.60157671656),
    (0.252418718401, 0.331052263829), (0.298663015648, 0.302646354002),
    (0.355749724218, 0.303020650651), (0.403718978315, 0.33867711083),
    (0.352507175597, 0.349987615384), (0.296791759886, 0.350478978225),
    (0.631326076346, 0.334136672344), (0.679073381078, 0.29645404267),
    (0.73597236153, 0.294721285802), (0.782865376271, 0.321305281656),
    (0.740312274764, 0.341849376713), (0.68499850091, 0.343734332172),
    (0.353167761422, 0.746189164237), (0.414587777921, 0.719053835073),
    (0.477677654595, 0.706835892494), (0.522732900812, 0.717092275768),
    (0.569832064287, 0.705414478982), (0.635195811927, 0.71565572516),
    (0.69951672331, 0.739419187253), (0.639447159575, 0.805236879972),
    (0.576410514055, 0.835436670169), (0.525398405766, 0.841706377792),
    (0.47641545769, 0.837505914975), (0.41379548902, 0.810045601727),
    (0.380084785646, 0.749979603086), (0.477955996282, 0.74513234612),
    (0.523389793327, 0.748924302636), (0.571057789237, 0.74332894691),
    (0.672409137852, 0.744177032192), (0.572539621444, 0.776609286626),
    (0.5240106503, 0.783370783245), (0.477561227414, 0.778476346951)])
    TPL_MIN, TPL_MAX = np.min(TEMPLATE, axis=0), np.max(TEMPLATE, axis=0)
    MINMAX_TEMPLATE = (TEMPLATE - TPL_MIN) / (TPL_MAX - TPL_MIN)
    npLandmarks=np.float32(landmarks)
    npLandmarkIndices = np.array([36,45,33])
    H = cv2.getAffineTransform(npLandmarks[npLandmarkIndices], imgDim * MINMAX_TEMPLATE[npLandmarkIndices])
    thumbnail = cv2.warpAffine(rgbImg, H, (imgDim, imgDim)) 
    return thumbnail   

def rotate(img, angle):
    rows, cols, a = img.shape
    img_rotated = cv2.warpAffine(img, (cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)),(cols, rows))
    return img_rotated       
    
def get_frames(vid):
    angle = 0
    faces = []
    frames = []
    angle_defined = False
    cam = cv2.VideoCapture(vid)
    next_frame_exists = True
    vid_frames = []

    i=1
    read_flag, frame = cam.read()
    while (read_flag):
        if i%10==0:
            vid_frames.append(frame)
            if frame is None:
                continue
            if angle_defined:
                frame = rotate(frame, angle)

                # cv2.imshow("image", img)
                # cv2.waitKey(0)

                faces = detect(frame)
            else:
                test_faces = []
                test_angle = 0
                while test_angle < 360:
                    test_img = rotate(frame, test_angle)
                    faces = detect(test_img)
                    if len(faces) > 0:
                        test_faces.append([faces, test_angle, test_img])
                    test_angle += 90
                if len(test_faces) == 1:
                    faces = test_faces[0][0]
                    angle = test_faces[0][1]
                    frame = test_faces[0][2]
                elif len(test_faces) == 2:
                    face1 = test_faces[0][0]
                    face2 = test_faces[1][0]
                    rectangle1 = face1[0][0][0]
                    rectangle2 = face2[0][0][0]

                    x1, y1, w1, h1 = rect_to_bb(rectangle1)
                    x2, y2, w2, h2 = rect_to_bb(rectangle2)
                    rect_summ1 = x1 - y1
                    rect_summ2 = x2 - y2
                    #
                    # data1 = face1[0][1][0]
                    # data2 = face2[0][1][0]
                    if rect_summ1 > rect_summ2:
                        faces = face2
                        angle = test_faces[1][1]
                        frame = test_faces[1][2]
                    else:
                        faces = face1
                        angle = test_faces[0][1]
                        frame = test_faces[0][2]

                if len(faces) > 0:
                    angle_defined = True
            if len(faces) == 1:
                frames.append([frame, faces[0]])
            # else: #- if face not found, read next 9 frames
            #     for flag in range (8):
            #         read_flag, frame = cam.read()
            #         if frame is None:
            #             continue
            #         if angle_defined:
            #             frame = rotate(frame, angle)
            #             faces = detect(frame)
            #             # cv2.imshow("image", frame) #- use for tests
            #             # cv2.waitKey(0)
            #             if len(faces) == 1:
            #                 frames.append([frame, faces[0]])
            #         i+=1

        read_flag,frame=cam.read()
        i+=1
    cam.release()

    # # Surta code
    # while next_frame_exists:
    #
    #     next_frame_exists, img = cam.read()
    #     vid_frames.append(img)
    #     # cv2.imshow("image", img)
    #     # cv2.waitKey(0)
    #     # color = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #     # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #     # cv2.imshow("color", color)
    #     # cv2.waitKey(0)
    #     # cv2.imshow("gray", gray)
    #     # cv2.waitKey(0)
    #
    #     if img is None:
    #         continue
    #     if angle_defined:
    #         img = rotate(img, angle)
    #
    #         # cv2.imshow("image", img)
    #         # cv2.waitKey(0)
    #
    #         faces = detect(img)
    #     else:
    #         # add face check
    #         # cv2.imshow("image", img)
    #         # cv2.waitKey(0)
    #         test_faces=[]
    #         test_angle=0
    #         while test_angle<360:
    #             test_img = rotate(img, test_angle)
    #             faces = detect(test_img)
    #             if len(faces)>0:
    #                 test_faces.append([faces,test_angle,test_img])
    #             test_angle += 90
    #         if len(test_faces)==1:
    #             faces=test_faces[0][0]
    #             angle=test_faces[0][1]
    #             img = test_faces[0][2]
    #         else:
    #             face1 = test_faces[0][0]
    #             face2 = test_faces[1][0]
    #             rectangle1=face1[0][0][0]
    #             rectangle2=face2[0][0][0]
    #
    #             x1, y1, w1, h1 = rect_to_bb(rectangle1)
    #             x2, y2, w2, h2 = rect_to_bb(rectangle2)
    #             # rect_summ1 = w1 - h1
    #             # rect_summ2 = w2 - h2
    #             #
    #             # data1 = face1[0][1][0]
    #             # data2 = face2[0][1][0]
    #             # if data2[1][0]>data1[1][0] and data2[0][1]>data1[0][1]:
    #             if w1>w2:
    #                 faces = face2
    #                 angle = test_faces[1][1]
    #                 img = test_faces[1][2]
    #             else:
    #                 faces = face1
    #                 angle = test_faces[0][1]
    #                 img = test_faces[0][2]
    #
    #         if len(faces) > 0:
    #             angle_defined = True
    #     if len(faces) == 1:
    #         # cv2.imshow("image", img) - use for tests
    #         # cv2.waitKey(0)
    #
    #         frames.append([img, faces[0]])
    frames_to_process = []
    i=0
    while i < len(frames):
        frames_to_process.append(frames[i])
        #i += int(len(frames)/10)
        i+=1
    return frames_to_process

def create_image_samples(img):
    samples = []


    #test results
    #bgr2rgb+img/1.5 = 0.5559
    #bgr2rgb+img*1.5 = 0.5754
    #bgr2rgb+img*1.5+img/1.5 = 0.5559
    #img+bgr2rgb+img*1.5+img/1.5 = 0.5559

    #test results

    samples.append(img) # - 0.68
    samples.append(cv2.cvtColor(img,cv2.COLOR_BGR2RGB)) #- 0.59
    # samples.append(cv2.equalizeHist(cv2.cvtColor(img,cv2.COLOR_BGR2GRAY))) # - not working

    # img1Color = (cv2.equalizeHist(cv2.cvtColor(img,cv2.COLOR_BGR2GRAY))) #-0.63
    # img1Color = cv2.cvtColor(img1Color, cv2.COLOR_GRAY2RGB)
    # samples.append(img1Color)
    #
    # img1 = img.astype(np.float32) # -0.68
    # img1Color = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    # img1Color = cv2.cvtColor(img1Color,cv2.COLOR_GRAY2RGB)
    # samples.append(img1Color)

    #
    img1 = img/1.5
    img1 = img1.astype(np.float32)
    img1Color=cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    samples.append(img1) # -0.68
    samples.append(img1Color) # - 0.56
    # # #
    img1 = img * 1.5
    img1 = img1.astype(np.float32)
    img1Color = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    samples.append(img1Color) # 0.63


    #samples.append(img*1.5)
    #samples.append(img/1.5)
    return samples

def get_matching_features(photo):
    # img = Image.open(photo)
    # img=np.array(img,'uint8')

    img = cv2.imread(photo,1)

##
    rects = detector(img, 0)
    if len(rects) == 0:
        angle = 0
        while len(rects) == 0 and angle < 360:
            angle += 90
            img = rotate(img, angle)
            rects = detector(img, 0)
    if len(rects) != 1:
        return None
    
    samples = create_image_samples(img)
    
    matching_reps = []
    shape = predictor(img, rects[0])
    shape = shape_to_np(shape)
    face = [[rects[0]], [shape]]
    for img_sample in samples:
        matching_reps.append(getRep(img_sample,face))
    return matching_reps

def get_distance(reps, matching_features):
    distances=[]
    for matching_rep in matching_features:
        for rep in reps:
            distances.append(np.linalg.norm(rep-matching_rep))
    sorted_distances=sorted(distances)
    return sorted_distances[0]

def compare(frame, matching_features):
    img = frame[0]
    face = frame[1]
    samples = create_image_samples(img)
    reps = []
    for img_sample in samples:
        reps.append(getRep(img_sample,face))
    distance=get_distance(reps,matching_features)
    return distance

def process(photo,vid):
    frames = get_frames(vid)
    passport_features = get_matching_features(photo)
    distances=[]
    for frame in frames:
        distances.append(compare(frame, passport_features))
    print(distances)
    if len(distances)>0:
        d = np.median(np.array(distances))
        print("median d = {}".format(str(d)))
        if d < 0.65:
            result = 'Accepted'
        else:
            result = 'Rejected'
        return result
    else:
        return None
    
def detect(img):
    faces = []
    rects = detector(img,0)
    for (i, rect) in enumerate(rects):
        shape = predictor(img, rect)
        shape = shape_to_np(shape)
        faces.append([[rect], [shape]])
    return faces
    
def shape_to_np(shape, dtype = "int"):
    coords = np.zeros((68, 2), dtype = dtype)
    for i in range(0, 68):
        coords[i] = (shape.part(i).x, shape.part(i).y)
    return coords     

def rect_to_bb(rect):
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y
    return (x, y, w, h)

def cmd(t, filedir, workdir, image):
    return ['/usr/bin/env','python', os.path.join(filedir, 'alfa.py'), '-frame', t,
            '--filedir', filedir, '-workdir', workdir, '-img', image]                  

filedir = "/home/den/openface"
workdir = os.path.join(filedir, 'alfa/')
videodir = os.path.join(workdir, 'videos/')
photodir = os.path.join(workdir, 'photos/')
modelDir = os.path.join(filedir, 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
classifierModel = os.path.join(workdir, 'classifier.pkl')
dlibFacePredictor = os.path.join(dlibModelDir, "shape_predictor_68_face_landmarks.dat")
openfaceModelDir = os.path.join(modelDir, 'openface')
predictor = dlib.shape_predictor(dlibFacePredictor)
detector = dlib.get_frontal_face_detector()   
networkModel=os.path.join(openfaceModelDir, 'nn4.small2.v1.t7')
net=openface.TorchNeuralNet(networkModel, '96', False)

@app.route('/', methods=['POST'])
def upload_file():
    """
     This is the face recognition API
     ---
     tags:
        - Face recognition
     consumes:
        - multipart/form-data
     parameters:
        - in: formData
          name: image
          type: file
          description: "Upload image with face"
          required: true
        - in: formData
          name: video
          type: file
          description: "Upload the video"
          required: true
     responses:
        500:
            description: Error occured
        200:
            description: Photo with face
            schema:

            """


    if request.method == 'POST':
        photo = request.files['image']
        vid = request.files['video']

        pathlib.Path(workdir).mkdir(parents=True,exist_ok=True)

        vid.save(os.path.join(workdir,'vid.mp4'))
        vid_path=(os.path.join(workdir,'vid.mp4'))
        photo_path=(os.path.join(workdir,'photo.jpg'))
        photo.save(os.path.join(workdir,'photo.jpg'))
        result = process(photo_path,vid_path)
        response = {'message': result} 
        response_pickled = jsonpickle.encode(response) 
        return Response(response=response_pickled, status=200, mimetype="application/json")
    # else:
    #     return "Try again!"
           
if __name__ == '__main__':
    app.run(host ='127.0.0.1', port=4568)


# In[ ]:



