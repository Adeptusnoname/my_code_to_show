import random
import time
from autogame.monsters.monsters import *
from autogame.game_mechanic.game_mechanic import upgrade_line_is_ok
from autogame.game_mechanic.game_mechanic import hit
from autogame.errors.errors import GameSystemException

def day_encounter(Player):
    DAY_TYPE = {1: "Nothing", 2: "Monster", 3: "Lottery", 4: "Skill check"}
    what_happend = DAY_TYPE[random.randint(1, len(DAY_TYPE))]

    if what_happend == "Nothing":
        nothing_happend(Player)
    elif what_happend == "Lottery":
        lottery(Player)
    elif what_happend == "Monster":
        monster_day_event(Player)
    elif what_happend == "Skill check":
        skill_check(Player)
    else:
        return


def nothing_happend(Player):
    print("Это был обычный день, без приключений и опастностей. Вы наконец то смогли отдохнуть и выспаться.\n")
    Player.restore_health()


def skill_check(Player):
    all_quests = {}
    quest_count = 0
    quest_text = ""
    try:
        with open("quests.txt", mode='r') as f:
            for string in f:
                if string.startswith("-#"):
                    quest_count += 1
                    quest_check = string.split(" ")
                    quest_attribute = quest_check[1]
                    quest_hardness = quest_check[2].strip()
                elif string.startswith("-!"):
                    quest_reward = string.split(" ")
                    quest_reward = quest_reward[1].strip()
                    all_quests.update({quest_count: [quest_attribute, quest_hardness, quest_text, quest_reward]})
                    quest_text = ""
                else:
                    quest_text += string

        which_quest = random.randint(1, len(all_quests))
        print(all_quests[which_quest][2])
        attribute_to_check = all_quests[which_quest][1]
        player_attribute = Player.attribute.get(all_quests[which_quest][0])
        hardness_level = random.randint(-1, 1)  # -1 -light 1- hard
        attribute_to_check_count = int(attribute_to_check) + int(hardness_level)

        print("Вам нужно иметь {} {} чтобы пройти испытание".format(str(attribute_to_check_count),
                                                                    all_quests[which_quest][0]))
        if attribute_to_check_count <= player_attribute:
            print("Испытание пройдено.")
            reward = int(all_quests[which_quest][3])
            Player.add_exp(reward)
            Player.restore_health()
        else:
            print("Вы не прошли испытание.\n")
    except FileNotFoundError:
        print("File with quests not found")


def monster_day_event(Player):
    print("Вы встретили монстра")
    monster = get_monster(Player.level)
    print(monster)
    monster_fight(Player, monster)

def monster_fight(Player, monster):
    player_name_to_compare = Player.name
    if Player.attribute['ag'] >= monster.attribute['ag']:
        player1 = Player
        player2 = monster
        print('Игрок ходит первым')
    else:
        player1 = monster
        player2 = Player
        print('Монстр ходит первым')

    while player1.health > 0 and player2.health > 0:
        print("{} осталось {} hp".format(player1.name, player1.health))
        print("{} осталось {} hp\n".format(player2.name, player2.health))

        hit(player1, player2)
        if player2.health > 0:
            hit(player2, player1)
        time.sleep(3)

    if player1.name == player_name_to_compare and player1.health > 0:
        print("{} win".format(player1.name))
        player1.add_exp(player2.level * 20)
    elif player1.name != player_name_to_compare and player1.health < 0:
        print("{} win".format(player2.name))
        player2.add_exp(player1.level * 20)  # Note that player2 -is real player and player1 -is monster
    else:
        print("Monster wins.\n GAME OVER")
        exit(0)

def lottery(Player):
    REWARD = 10
    print(
        "Известный герой, встретивший вас в таверне, предложил поделиться опытом если вы отгадаете числа которые он загадал\n")
    your_numbers = [random.randint(1, 10), random.randint(1, 10)]
    his_numbers = [random.randint(1, 10), random.randint(1, 10)]
    your_reward = 0
    match_number = 0

    for i in your_numbers:
        if i in his_numbers:
            your_reward += REWARD
            match_number += 1

    print("Чисел угадано {}: ".format(match_number))
    Player.add_exp(your_reward)
    Player.restore_health()