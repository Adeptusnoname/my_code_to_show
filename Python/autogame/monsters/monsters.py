import random
from autogame.errors.errors import GameSystemException
from autogame.game_mechanic.game_mechanic import upgrade_line_is_ok


class Monster():
    BESTIALITY = ["Ork", "Peasant", "Wild Rat", "Spider", "Jelly cube"]

    def __init__(self, name, level, attribute={}):
        self.name = name
        self.level = level
        self.attribute = attribute
        self.check_paremeters()

    def __str__(self):
        return (
            '{} level {} \n  \n health = {} \n mana= {} \n *******Attribute******* \n {} \n *******Parameters*******\n crit chance={} % \n avoid_chance={} %\n hit_chance={} %\n damage={} \n'.format(
                self.name, self.level, self.health,
                self.manapoints,
                self.attribute,
                self.crit_chance,
                self.avoid_chance,
                self.hit_chance,
                self.damage))

    def check_paremeters(self):
        self.health = self.attribute['end'] * 6
        self.manapoints = self.attribute['int'] * 3
        self.crit_chance = 5 + self.attribute['per'] * 3  # in percent
        self.avoid_chance = 5 + self.attribute['ag'] * 3
        self.hit_chance = 50 + (5 + self.attribute['ag'] * 3) + (5 + self.attribute['per'] * 3)
        self.damage = self.attribute['str']

    def level_up(self):
        available_points = 2
        for i in range(1, self.level, 1):
            for attribute in self.upgrade_line:
                if self.upgrade_line.get(attribute) != 0:
                    if (i + 1) % self.upgrade_line.get(attribute) == 0 and int(
                            self.upgrade_line.get(attribute)) <= (
                                i + 1) and available_points > 0 and self.upgrade_line.get(attribute) != 1:
                        new_attribute = self.attribute.get(attribute) + 1
                        self.attribute.update({attribute: new_attribute})
                        available_points -= 1

            # add available points to main attribute
            while available_points > 0:
                for attribute in self.upgrade_line:
                    if self.upgrade_line.get(attribute) == 1 and available_points > 0:
                        new_attribute = self.attribute.get(attribute) + 1
                        self.attribute.update({attribute: new_attribute})
                        available_points -= 1
            available_points = 2


class Ork(Monster):
    def __init__(self, level):
        super().__init__("Ork", level, attribute={'int': 1, 'ag': 1, 'str': 4, 'end': 1, 'per': 1})
        self.upgrade_line = {'int': 15, 'ag': 3, 'str': 1, 'end': 2, 'per': 4}


class Peasant(Monster):
    def __init__(self, level):
        super().__init__("Agressive peasant", level, attribute={'int': 1, 'ag': 1, 'str': 2, 'end': 2, 'per': 1})
        self.upgrade_line = {'int': 15, 'ag': 3, 'str': 1, 'end': 2, 'per': 4}


class Wild_Rat(Monster):
    def __init__(self, level):
        super().__init__("Wild rat", level, attribute={'int': 1, 'ag': 2, 'str': 1, 'end': 1, 'per': 1})
        self.upgrade_line = {'int': 15, 'ag': 1, 'str': 2, 'end': 3, 'per': 4}


class Spider(Monster):
    def __init__(self, level):
        super().__init__("Big spider", level, attribute={'int': 1, 'ag': 2, 'str': 1, 'end': 1, 'per': 2})
        self.upgrade_line = {'int': 10, 'ag': 2, 'str': 3, 'end': 4, 'per': 1}


class Jelly_cube(Monster):
    def __init__(self, level):
        super().__init__("Jelly cube", level, attribute={'int': 0, 'ag': 0, 'str': 1, 'end': 4, 'per': 2})
        self.upgrade_line = {'int': 0, 'ag': 3, 'str': 2, 'end': 1, 'per': 4}


def get_monster(hero_level):
    monster_num = random.randint(0, len(Monster.BESTIALITY) - 1)
    min_monster_level = max(1, hero_level - 1)
    monster_level = random.randint(min_monster_level, hero_level + 1)

    if Monster.BESTIALITY[monster_num] == "Ork":
        monster = Ork(monster_level)
    elif Monster.BESTIALITY[monster_num] == "Peasant":
        monster = Peasant(monster_level)
    elif Monster.BESTIALITY[monster_num] == "Wild Rat":
        monster = Wild_Rat(monster_level)
    elif Monster.BESTIALITY[monster_num] == "Spider":
        monster = Spider(monster_level)
    elif Monster.BESTIALITY[monster_num] == "Jelly cube":
        monster = Jelly_cube(monster_level)
    else:
        raise GameSystemException("Unexpected monster")

    upgrade_line_is_ok(monster)
    monster.level_up()
    monster.check_paremeters()
    return monster
