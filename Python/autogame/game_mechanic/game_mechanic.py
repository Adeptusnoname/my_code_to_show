import random
from autogame.errors.errors import GameSystemException

def upgrade_line_is_ok(Creature):
    attribute_zero_flag = 0
    for attribute in Creature.upgrade_line:
        if Creature.upgrade_line.get(attribute) < 0:
            raise GameSystemException("Upgrade line attribute below zero")
        elif Creature.upgrade_line.get(attribute) == 0:
            attribute_zero_flag += 1
            if attribute_zero_flag == len(Creature.upgrade_line):
                raise GameSystemException("All upgrade line attribute is 0")

    if 1 not in Creature.upgrade_line.values():
        raise GameSystemException("No central attribute in upgrade line attribute")

def hit(player1, player2):
    player1_hit_chance = player1.hit_chance - player2.avoid_chance
    player2_hit_chance = player2.hit_chance - player1.avoid_chance
    if random.randint(0, 100) <= player1_hit_chance:
        if random.randint(0, 100) <= player1.crit_chance:
            player2.health -= player1.damage * 2
            print("{} выбил крит и нанес {} урона".format(player1.name, player1.damage * 2))
        else:
            print("{} нанес {} урона".format(player1.name, player1.damage))
            player2.health -= player1.damage
    else:
        print("{} промахнулся".format(player1.name))