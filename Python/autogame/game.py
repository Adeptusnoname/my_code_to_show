import time
from autogame.heroes.heroes import *
from autogame.day_events.day_event import day_encounter
from autogame.game_mechanic.game_mechanic import upgrade_line_is_ok

if __name__ == '__main__':

    print("Выберите класс персонажа")
    for i, hero_class in enumerate(Player.CLASSES):
        print(i, hero_class)

    choice = int(input(""))
    name = input("Введите имя героя\n")

    if choice == 0:
        user = Warrior(name)
    elif choice == 1:
        user = Thief(name)
    elif choice == 2:
        user = Barbarian(name)
    elif choice == 3:
        user = Willage_Fool(name)
    else:
        print("Wrong input. Restart the game")
        exit(0)

    upgrade_line_is_ok(user)
    print('Ваш персонаж:\n')
    print(user)

    print('Итак, ваше приключение начинается:\n')

    while True:
        day_encounter(user)
        print(user)
        time.sleep(5)
