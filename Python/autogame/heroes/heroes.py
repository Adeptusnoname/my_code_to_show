class Player():
    MAX_LEVEL = 10
    LEVEL_UP_SYSTEM = {x // 100: x for x in range(0, (MAX_LEVEL + 1) * 100, 100)}
    CLASSES = ["Warrior", "Thief", "Barbarian", "Willage Fool"]

    def __init__(self, name, level=1, attribute={}):
        self.expirence = 0
        self.level = level
        self.attribute = attribute
        self.name = name
        self.check_paremeters()

    def check_paremeters(self):
        self.health = self.attribute['end'] * 6
        self.manapoints = self.attribute['int'] * 3
        self.crit_chance = 5 + self.attribute['per'] * 3  # in percent
        self.avoid_chance = 5 + self.attribute['ag'] * 3
        self.hit_chance = 50 + (5 + self.attribute['ag'] * 3) + (5 + self.attribute['per'] * 3)
        self.damage = self.attribute['str']
        self.max_health = self.health

    def add_exp(self, exp):
        print("Получено {} опыта\n".format(exp))
        self.expirence += exp
        if self.expirence >= self.LEVEL_UP_SYSTEM[self.level]:
            self.expirence = self.expirence - self.LEVEL_UP_SYSTEM[self.level]
            self.level_up()
            print('Был получен уровень {}\n'.format(self.level))

    def __str__(self):
        return (
            '{} level {} \n experience {} \n health = {} \n mana= {} \n *******Attribute******* \n {} \n *******Parameters*******\n crit chance={} % \n avoid_chance={} %\n hit_chance={} %\n damage={} \n'.format(
                self.name, self.level, self.expirence, self.health,
                self.manapoints,
                self.attribute,
                self.crit_chance,
                self.avoid_chance,
                self.hit_chance,
                self.damage))

    def restore_health(self):
        if self.health + 3 < self.max_health:
            self.health += 3
            print("Восстановлено 3 здоровья\n")
        else:
            self.health = self.max_health
            print("Здоровье восполнено до максимума\n")

    def level_up(self):
        available_points = 3
        if self.level < self.MAX_LEVEL:
            for attribute in self.upgrade_line:
                if self.upgrade_line.get(attribute) != 0:
                    if (self.level + 1) % self.upgrade_line.get(attribute) == 0 and int(
                            self.upgrade_line.get(attribute)) <= (
                                self.level + 1) and available_points > 0 and self.upgrade_line.get(attribute) != 1:
                        new_attribute = self.attribute.get(attribute) + 1
                        self.attribute.update({attribute: new_attribute})
                        available_points -= 1

                        # add available points to main attribute
            while available_points > 0:
                for attribute in self.upgrade_line:
                    if self.upgrade_line.get(attribute) == 1 and available_points > 0:
                        new_attribute = self.attribute.get(attribute) + 1
                        self.attribute.update({attribute: new_attribute})
                        available_points -= 1
            self.level += 1
            self.check_paremeters()
        else:
            print("Hero already reached MAX Level (level {}):".format(
                self.MAX_LEVEL))  # Just to be sure if something will go wrong in standart level up situation. Also can be used if Max level is not the game end.
            print('Congratulations!!!\n you win this game')
            exit(0)

        if self.level >= self.MAX_LEVEL:
            print('Congratulations!!!\n you win this game')
            exit(0)


class Warrior(Player):
    def __init__(self, name):
        super().__init__('Warrior ' + name, attribute={'int': 2, 'ag': 2, 'str': 4, 'end': 2, 'per': 3})
        self.upgrade_line = {'int': 5, 'ag': 3, 'str': 1, 'end': 2, 'per': 4}


class Thief(Player):
    def __init__(self, name):
        super().__init__('Thief ' + name, attribute={'int': 2, 'ag': 3, 'str': 3, 'end': 2, 'per': 3})
        self.upgrade_line = {'int': 5, 'ag': 1, 'str': 2, 'end': 4, 'per': 3}


class Barbarian(Player):
    def __init__(self, name):
        super().__init__('Barbarian ' + name, attribute={'int': 2, 'ag': 2, 'str': 5, 'end': 2, 'per': 2})
        self.upgrade_line = {'int': 10, 'ag': 3, 'str': 1, 'end': 2, 'per': 4}


class Willage_Fool(Player):
    def __init__(self, name):
        super().__init__('Willage Fool ' + name, attribute={'int': 0, 'ag': 2, 'str': 4, 'end': 4, 'per': 2})
        self.upgrade_line = {'int': 10, 'ag': 2, 'str': 1, 'end': 1, 'per': 4}
