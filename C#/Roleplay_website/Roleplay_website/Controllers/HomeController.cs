﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Roleplay_website.Models;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;


namespace Roleplay_website.Controllers
{
    public class HomeController : Controller
    {
        WebsiteContext dbW = new WebsiteContext();
        ApplicationdbContext db = new ApplicationdbContext();

        //Start: Templates
        [Authorize]
        public ActionResult My_templates()
        {
            // получаем из бд все объекты Book
            IEnumerable<Template> templates = dbW.Templates;
            // передаем все объекты в динамическое свойство Books в ViewBag
            ViewBag.Templates = templates;
            // возвращаем представление
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult New_template(Template template)
        {

            template.User_owner = User.Identity.Name;
            if (template.Is_base == null)
            {
                template.Is_base = false;
            }
            // добавляем информацию о покупке в базу данных
            dbW.Templates.Add(template);
            // сохраняем в бд все изменения
            dbW.SaveChanges();

            return Redirect("/Home/My_templates");
        }

        [Authorize]
        [HttpGet]
        public ActionResult New_template()
        {
            ViewBag.TemplateId = dbW.Templates.Count() + 1;
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Show_Template(int? id)
        {

            Template template_to_show = dbW.Templates.Find(id);

            if (id == null)
            {
                return HttpNotFound();
            }

            if (template_to_show == null)
            {
                return HttpNotFound();
            }

            if (User.IsInRole("admin") == true || (User.Identity.Name == template_to_show.User_owner) || (template_to_show.Is_base == true))
            {              
                return View(template_to_show);
            }
            else
            {
                return new HttpStatusCodeResult(403, "В доступе отказано");
            }

            return View(); //just to be sure if something will changed
        }

        [Authorize]
        [HttpPost] //HttpPut
        public ActionResult Show_Template(Template template) //-update template
        {
            if (User.IsInRole("admin") == true || (template.Is_base != true && User.Identity.Name == template.User_owner) || (template.Is_base == true && User.IsInRole("moderator") == true)) 
            {
                dbW.Entry(template).State = EntityState.Modified;
                dbW.SaveChanges();
            }
            else
            {
                return new HttpStatusCodeResult(403, "В доступе отказано"); //permission violation
            }


            return Redirect("/Home/My_templates");
        }

        [Authorize]
        public ActionResult Show_some_templates(string name)
        {
            if (name == null)
            {
                name = "";
            }

            string owner = User.Identity.Name;


            var some_templates = dbW.Templates.Where(a => a.Name.Contains(name)).ToList();
            var base_templates = some_templates.Where(a => a.Is_base.Equals(true)).ToList();
            if (User.IsInRole("admin") == false)
            {
                var owned_teplates = some_templates.Where(a => a.User_owner == owner).ToList();
                var templates_to_show = base_templates.Union(owned_teplates).ToList();
                return PartialView(templates_to_show);
            }
            else
            {
                return PartialView(some_templates);
            }

        }

        [Authorize]
        public ActionResult Show_form_text(string text_Id)
        {
            if (text_Id == null)
            {
                return HttpNotFound(); 
            }


            var Id = Convert.ToInt16(text_Id);
            var templates = dbW.Templates.Where(a => a.Id.Equals(Id)).ToList();
     
            return Json(templates, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        [HttpGet] //HttpDelete
        public ActionResult Delete_Template(int? id)
        {

            if (id == null)
            {
                return HttpNotFound();
            }

            Template template_to_del = dbW.Templates.Find(id);
              //  .FirstOrDefault(c => c.Id == id);

            if (template_to_del == null) 
            {
                return HttpNotFound();
            }

            if (User.IsInRole("admin") == true || (template_to_del.Is_base != true && User.Identity.Name == template_to_del.User_owner))
            {
                return View(template_to_del);
            }
            else
            {
                return new HttpStatusCodeResult(403, "В доступе отказано");
            }
        }

        [Authorize]
        [HttpPost, ActionName("Delete_Template")] // to avoid security cases in "Get" method
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Template template = dbW.Templates.Find(id);

            if (template == null)
            {
                return HttpNotFound();
            }

            if (User.IsInRole("admin") == true || (template.Is_base != true && User.Identity.Name == template.User_owner) )
            {

                dbW.Templates.Remove(template);
                dbW.SaveChanges();
            }
            else
            {
                return new HttpStatusCodeResult(403, "В доступе отказано");
            }

            return RedirectToAction("/My_templates");
        }

        //End: Templates

        //Start: Characters

        [Authorize]
        public ActionResult My_characters()
        {
            // получаем из бд все объекты
            IEnumerable<Character> characters = dbW.Characters;
            // передаем все объекты в динамическое свойства в ViewBag
            ViewBag.Characters = characters;
            // возвращаем представление
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult New_Character(Character character)
        {

            character.Owner = User.Identity.Name;
            // добавляем информацию о герое в базу данных
            dbW.Characters.Add(character);
            // сохраняем в бд все изменения
            dbW.SaveChanges();

            return Redirect("/Home/My_characters");
        }

        [Authorize]
        [HttpGet]
        public ActionResult New_character()
        {
            try
            {
                ViewBag.CharacterId = dbW.Characters.Count() + 1;
                ViewBag.Templates = dbW.Templates;

                return View();
            }
            catch (System.Data.SqlClient.SqlException)
            {
                return Redirect("/Home"); //return to base page.
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult Delete_character(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Character character_to_del = dbW.Characters.Find(id);

            if (character_to_del == null) 
            {
                return HttpNotFound();
            }

            if (User.IsInRole("admin") == true || (User.Identity.Name == character_to_del.Owner))
            {
                return View(character_to_del);
            }
            else
            {
                return new HttpStatusCodeResult(403, "В доступе отказано");
            }
        
        }

        [Authorize]
        [HttpPost, ActionName("Delete_character")] // to avoid security cases in "Get" method
        public ActionResult DeleteCharacterConfirmed(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Character character_to_del = dbW.Characters.Find(id);

            if (character_to_del == null)
            {
                return HttpNotFound();
            }

            if (character_to_del.Owner == User.Identity.Name || User.IsInRole("admin") == true)
                    {
                        dbW.Characters.Remove(character_to_del);
                        dbW.SaveChanges();

                        return Redirect("/Home/My_characters");
                    }
                    else
                    {
                        return new HttpStatusCodeResult(403, "В доступе отказано");
                    }
        }

        [Authorize]
        [HttpGet]
        public ActionResult Show_Character(int? id)
        {

            if (id == null)
            {
                return HttpNotFound();
            }

            Character character_to_show = dbW.Characters.Find(id);

            if (character_to_show == null)
            {
                return HttpNotFound();
            }


            if ((User.IsInRole("admin") == true) || (User.Identity.Name == character_to_show.Owner))
            {

                return View(character_to_show);
            }
            else
            { return new HttpStatusCodeResult(403, "В доступе отказано"); }
                
            return View();
        }

        [Authorize]
        [HttpPost] //HttpPut
        public ActionResult Show_Character(Character character)
        {
            if ((User.IsInRole("admin") == true) || (User.Identity.Name == character.Owner))
            {
                dbW.Entry(character).State = EntityState.Modified;
                dbW.SaveChanges();
            }
            else
            {
                return new HttpStatusCodeResult(403, "В доступе отказано");           
            }

            return Redirect("/Home/My_characters");
        }

        [Authorize]
        public ActionResult Show_some_characters(string name)
        {
            if (name == null)
            {
                name = "";
            }

            string owner = User.Identity.Name;


            var some_characters = dbW.Characters.Where(a => a.Hero_name.Contains(name)).ToList();
            if (User.IsInRole("admin") == false)
            {
                var owned_teplates = some_characters.Where(a => a.Owner == owner).ToList();
                return PartialView(owned_teplates);
            }
            else
            {
                return PartialView(some_characters);
            }

        }

        //End: Characters

        
        
        //Start:Administration


        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Update_user(string roles, string userId)
        {

           

            if (roles!=null)
            {
                List<string> new_roles = new List<string>(roles.Split(','));
                
                for (var i=0;i<new_roles.Count;i++)
                {
                    if (new_roles[i] == "")
                    {
                        new_roles.RemoveAt(i);
                        i--;
                    }
                }

                ApplicationUser user = db.Users.Find(userId);

                var userManager = new ApplicationManager(new UserStore<ApplicationUser>(db));

                var user_roles = userManager.GetRoles(userId).ToList();

                    foreach (var global_role in db.Roles.ToList())
                    {
                                        
                        if (user_roles.Contains(global_role.Name))
                        {
                            if (new_roles.Contains(global_role.Name))
                            { //nothing to change
                            }
                            else
                            {
                                userManager.RemoveFromRoleAsync(userId, global_role.Name);
                            }
                        }
                        else
                        {
                            if (new_roles.Contains(global_role.Name))
                            {
                                userManager.AddToRoleAsync(userId, global_role.Name);
                            }
                            else
                            {
                            }
                        }

                    }

                    //dbW.Entry(user).State = EntityState.Modified;
                    //dbW.SaveChanges();

                }
            else
            {
               return HttpNotFound();
            }

            return Content("/Home/Administration");
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Show_user(string Id)
        {
            if (Id == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user_to_show = db.Users.Find(Id);

            if (user_to_show == null)
            {
                return HttpNotFound();
            }


            var all_roles = db.Roles.ToList();
            ViewBag.all_roles = all_roles;

            return View(user_to_show);
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Administration()
        {
            List<ApplicationUser> users = db.Users.ToList();
            ViewBag.users = users;

            var all_roles = db.Roles.ToList();
            ViewBag.all_roles = all_roles;

            foreach (var user in users)
            {
                var name = user.UserName;
                var id = user.Id;
                
                var roles = user.Roles.ToList();


                foreach (var role in roles)
                { 
                    string user_role = role.Role.Name;
                }


            }
            // возвращаем представление
            return View(users);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Show_users_roles(string name)
        {
            if (name == null)
            {
                name = "";
            }

            var users = db.Users.Where(a => a.UserName.Contains(name)).ToList();
            ViewBag.all_roles = db.Roles.ToList();
            var global_roles = db.Roles.ToList();
            foreach (var role in global_roles)
            { 
            }

            return PartialView(users);

        }


        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult Delete_user(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ViewBag.all_roles = db.Roles.ToList();

            ApplicationUser user_to_del = db.Users.Find(id);

            if (user_to_del == null)
            {
                return HttpNotFound();
            }

            return View(user_to_del);
          
        }

        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete_user")] // to avoid security cases in "Get" method
        public ActionResult DeleteUserConfirmed(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user_to_del = db.Users.Find(id);
            var userManager = new ApplicationManager(new UserStore<ApplicationUser>(db));

            if (user_to_del == null)
            {
                return HttpNotFound();
            }

            userManager.DeleteAsync(user_to_del);

                return Redirect("/Home/Administration");
        }

        //End: Administration

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Index()
        {

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            dbW.Dispose();
            base.Dispose(disposing);
        }

    }
}