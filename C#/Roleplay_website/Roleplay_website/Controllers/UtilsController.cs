﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Roleplay_website.Utils;

namespace Roleplay_website.Controllers
{
    public class UtilsController : Controller
    {
        //
        // GET: /My/
        public ActionResult Index()
        {
            return View();
        }

        public string ThrowDice(int dice)
        {
            int dice_result;
            Random rnd = new Random();
            dice_result = rnd.Next(dice);
            var dicestr = Convert.ToString(dice_result);
            return "" + dicestr + "";         
        }

        public ActionResult GetDicer()
        {
            return new Dicer("http://www.wizards.com/dnd/dice/dice.htm");
        }

	}
}