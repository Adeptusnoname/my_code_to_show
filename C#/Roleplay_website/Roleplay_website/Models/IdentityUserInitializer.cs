﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Roleplay_website.Models;
using Roleplay_website.Controllers;
using Microsoft.Owin;




namespace Roleplay_website.Models
{
    public class IdentityUserInitializer : CreateDatabaseIfNotExists<ApplicationdbContext>
    {

        protected override void Seed(ApplicationdbContext db)
        {
            var userManager = new ApplicationManager(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var adminrole = new IdentityRole{ Id = "adminrole", Name = "admin" };
            var userrole = new IdentityRole { Id = "userrole", Name = "user" };
            roleManager.Create(adminrole);
            roleManager.Create(userrole);

            var admin = new ApplicationUser{UserName="admin",Id="adminid1"};
            string password = "2789087test";
            var result = userManager.Create(admin,password);

            if (result.Succeeded)
            {

                userManager.AddToRole(admin.Id, adminrole.Name);
            
            }

           // ApplicationUser admin= new ApplicationUser {UserName="admin",Id="adminid1"};
           // IdentityRole role_for_admins = new IdentityRole { Id = "adminrole", Name = "admin" };
           // IdentityUserRole adminrole = new IdentityUserRole { Role = role_for_admins, RoleId = "adminrole", User = admin, UserId = "admin1" };


           // userManager.AddPasswordAsync(admin.Id, "2789087");
           // userManager.AddToRole(admin.Id,adminrole.Name);

           // db.SaveChanges();

            base.Seed(db);
        }

    }
}