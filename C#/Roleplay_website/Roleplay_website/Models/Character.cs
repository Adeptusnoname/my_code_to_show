﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Roleplay_website.Models
{
    public class Character
    {

        public int Id { get; set; } // unique id

        public string Game { get; set; } //  in which game it used

        public string Owner { get; set; } // user owner

        public string Hero_blank { get; set; } // blank with whole info

        public string Hero_text { get; set; } // Short info 
        
        public string Hero_name { get; set; } // Hero name

        public string Hero_status { get; set; } // Dead, Game ended.

    }
}