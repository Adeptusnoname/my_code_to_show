﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Roleplay_website.Models
{
    public class Game
    {

        public int id { get; set; } //unique game id

        public string name { get; set; } // should be unique for game master

        public string master { get; set; } // game master

        public string[] players { get; set; } //game players ( can be changed by master)

        public string game_info { get; set; } // game info

        public string game_status { get; set; } // closed, in progress, etc.

    }
}