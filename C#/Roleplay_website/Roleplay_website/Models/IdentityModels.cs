﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Roleplay_website.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
 
    
    }

    public class ApplicationdbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationdbContext()
            : base("Website_administration")
        {

          //  Database.SetInitializer<ApplicationdbContext>(new IdentityUserInitializer());

        }

        public static ApplicationdbContext Create()
        {
            return new ApplicationdbContext();
        }

    }

}