﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Roleplay_website.Models
{
    public class Template
    {

    [ScaffoldColumn(false)]
    public int Id {get;set;} //global database id

    [Display(Name = "Название")]
    [UIHint("String")]
    public string Name { get; set; } // template name. Should be unique for user

    [Display(Name = "Тип шаблона")]
    [UIHint("Boolean")]
    public bool Is_base { get; set; } // for templates which will be duplicated for all users

    [Display(Name = "Описание шаблона")]
    [UIHint("MultilineText")]
    public string Template_text { get; set; } // template text in txt ( html in future)

    [Display(Name = "Текст шаблона")]
    [DataType(DataType.MultilineText)]
    public string Template_form { get; set; } // template form in txt ( html in future)

    [Display(Name = "Ссылка на книгу правил")]
    [UIHint("Url")]
    public string Rulebook_link { get; set; } // link to rule book

    
    [Display(Name = "Создатель шаблона")]
    [UIHint("String")]
    public string User_owner { get; set; } // Owner name.

    }
}