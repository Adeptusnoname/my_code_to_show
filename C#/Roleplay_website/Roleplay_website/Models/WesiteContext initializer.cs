﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Roleplay_website.Models
{
    public class WesiteContext_initializer : CreateDatabaseIfNotExists<WebsiteContext>
    {

        protected override void Seed(WebsiteContext db)
        {

            Character chartest = new Character { Hero_name = "test", Id = 1, Owner = "none" ,Game="",Hero_blank="",Hero_status="test",Hero_text="test"};
            db.Characters.Add(chartest);

            Template templtest = new Template { Id = 1, Is_base = false, Name = "Empty template", Rulebook_link = "", Template_form = "", Template_text="",User_owner="admin"};

            db.SaveChanges();
        }

    }
}