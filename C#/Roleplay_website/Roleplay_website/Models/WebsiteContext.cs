﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Roleplay_website.Models
{

    public class WebsiteContext: DbContext
    {
        public DbSet<Template> Templates { get; set; }
        public DbSet<Character> Characters { get; set; }

        static WebsiteContext()
        {            
        }

    }
 

}