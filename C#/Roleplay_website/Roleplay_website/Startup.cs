﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Roleplay_website.Startup))]
namespace Roleplay_website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
