﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            listBox1.Items.Add("Вся_беларусь");
            listBox1.Items.Add("Минск");
        }


        public void kufar_connector(string website)// connect to website
        {
            // 
            string full_request;
            full_request = "";
            string city = "";

            try
            {
                city=listBox1.SelectedItem.ToString();
            }
            catch (NullReferenceException e)
            {
                MessageBox.Show("Локация не выбрана. Применены дефолтные настройки.", "Не выбрана локация", MessageBoxButtons.OK);
            }

            switch (city)
            {
                case "Вся_беларусь":
                    full_request = website + "/беларусь";
                    break;

                case "Минск":
                    full_request = website + "/минск_город";
                    break;
                default:
                    full_request = website + "/беларусь";
                    break;
            }
                 
            if (textBox2.Text != "search text")
            {
                full_request = full_request  +"/"+ textBox2.Text;
            }

            // Выполняем запрос по адресу и получаем ответ в виде строки

                using (var webClient = new WebClient())
                {
                    var response = "";
                webClient.Encoding = Encoding.UTF8;
                try
                {
                    response = webClient.DownloadString(website);
                }
                catch(WebException ex)
                {
                    MessageBox.Show("No internet connection","No internet connection or page not found");    
                }

                richTextBox1.Text = response;
                }

        }
            


        private void button1_Click(object sender, EventArgs e)
        {
        kufar_connector(textBox1.Text);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
