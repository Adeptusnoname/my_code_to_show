﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{

    class Menu
    {
        public string[] menu_items;
        public string[] animal_items;
        public int cells;
        public Animal[] animal_in_cell;
        public Menu() 
        {
            cells = 0;
            animal_in_cell = new Animal[0];
            menu_items = new string[5];
            menu_items[0] = "add_animal";
            menu_items[1] = "show_info";
            menu_items[2] = "feed";
            menu_items[3] = "add cell";
            menu_items[4] = "exit";

            animal_items = new string[2];
            animal_items[0] = "Bear";
            animal_items[1]="Rabbit";
            
        }

        public void show_menu()
        {
            Console.WriteLine("Please choose what to do");
            for (int i = 0; i < menu_items.Length; i++)
                {
                    Console.WriteLine((i+1).ToString() + " " + menu_items[i]);
                    
                }
            string choiceStr=Console.ReadLine();

            switch (choiceStr)
            {   
                case "1": //add animal
                    Console.Clear();
                    int free_cells=0;

                    for (int cell = 0; cell < animal_in_cell.Length; cell++)
                    {
                        if (animal_in_cell[cell] == null)
                        {
                            free_cells++;
                        }
                    }

                    if (free_cells==0)
                    {
                        Console.WriteLine("You have no free cells\n");
                    }
                    else
                    {
                        Console.WriteLine("Please define which animal should be added:");
                        for (int i = 0; i < animal_items.Length; i++)
                        {
                            Console.WriteLine((i + 1).ToString() + " " + animal_items[i]);
                        }
                        string choice_animalStr = Console.ReadLine();

                        switch (choice_animalStr)
                        {
                            case "1": //bear
                                int choice_cell_b;
                                Console.WriteLine("Please enter bear name");
                                string bear_name = Console.ReadLine();
                                Bear new_bear = new Bear(bear_name);
                                new_bear.show_info();

                                Console.WriteLine("You have "+animal_in_cell.Length+" Cells");
                                Console.WriteLine("Please define in which cell it should be added");

                                try
                                {
                                    choice_cell_b = Convert.ToInt32(Console.ReadLine());
                                    if (choice_cell_b > animal_in_cell.Length || choice_cell_b < 0)
                                    {
                                        Console.WriteLine("Wrong input");
                                    }
                                    else
                                    {
                                        if (animal_in_cell[choice_cell_b - 1] == null)
                                        {
                                            animal_in_cell[choice_cell_b - 1] = new_bear;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Somebody already lived here");
                                        }

                                    }

                                }
                                catch
                                {
                                    Console.WriteLine("Please use numbers");
                                    Console.Clear();
                                }

                                    break;
                            case "2": //rabbit
                                int choice_cell_r;
                                Console.WriteLine("Please enter rabbit name");
                                string rabbit_name = Console.ReadLine();
                                Rabbit new_rabbit = new Rabbit(rabbit_name);
                                new_rabbit.show_info();

                                Console.WriteLine("You have "+animal_in_cell.Length+" Cells");
                                Console.WriteLine("Please define in which cell it should be added");

                                try
                                {
                                    choice_cell_r = Convert.ToInt32(Console.ReadLine());
                                    if (choice_cell_r > animal_in_cell.Length || choice_cell_r < 0)
                                    {
                                        Console.WriteLine("Wrong input\n");
                                    }
                                    else
                                    {
                                        if (animal_in_cell[choice_cell_r - 1] == null)
                                        {
                                            animal_in_cell[choice_cell_r - 1] = new_rabbit;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Somebody already lived here\n");
                                            if (animal_in_cell[choice_cell_r - 1].is_hungry == true && animal_in_cell[choice_cell_r - 1].can_be_food == false && animal_in_cell[choice_cell_r - 1].animal_size >= new_rabbit.animal_size && new_rabbit.can_be_food == true) //new_rabbit.can_be_food == true - additional check
                                            {
                                                Console.WriteLine("Oops!! something is wrong! your animal are eaten.");
                                                animal_in_cell[choice_cell_r - 1].is_hungry = false;
                                            }

                                        }

                                    }
                                }
                                catch
                                {
                                    Console.WriteLine("Please use numbers");
                                    Console.Clear();
                                }
                                break;

                            default:
                                Console.WriteLine("Wrong input\n");
                                break;
                        }
                    }
                    break;

                case "2": //show_info
                    Console.Clear();
                    for (int i=0; i < animal_in_cell.Length; i++)
                    {
                        if (animal_in_cell[i] != null)
                        {
                            Console.WriteLine("Cell " + (i + 1) + " contain:");
                            animal_in_cell[i].show_info();
                        }
                        else
                        {
                            Console.WriteLine("Cell " + (i + 1) + " are empty\n");
                        }
                    
                    }
                    break;
                case "3": //feed
                    Console.Clear();
                    free_cells = 0;
                    Console.WriteLine("Please select in which cell food should be delivered");
                    for (int i=0; i<animal_in_cell.Length;i++)
                        {
                            if (animal_in_cell[i] != null)
                            {
                                Console.WriteLine("Cell " + (i+1));
                                animal_in_cell[i].show_info();
                            }
                            else
                            {
                                free_cells++;
                                Console.WriteLine("Cell "+ (i+1) +" are empty.nobody can be feeded\n");

                            }
                        }
                    if (free_cells == animal_in_cell.Length)
                    {
                        Console.WriteLine("All cells are empty\n");
                    }
                    else
                    {
                        try
                        {
                            int choice = Convert.ToInt32(Console.ReadLine());

                            if (choice > animal_in_cell.Length || choice < 0)
                            {
                                Console.WriteLine("Wrong input\n");
                            }
                            else
                            {
                                if (animal_in_cell[choice - 1] != null)
                                {
                                    if (animal_in_cell[choice - 1].is_hungry == false)
                                    {
                                        Console.WriteLine("Animal is not hungry\n");
                                    }
                                    else
                                    {
                                        animal_in_cell[choice - 1].is_hungry = false;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Cell are empty.nobody can be feeded\n");
                                }
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Please use numbers");
                            Console.Clear();
                        }
                    }
                    break;
                case "4": //add cell
                    Console.Clear();
                    Array.Resize(ref animal_in_cell, animal_in_cell.Length + 1);
                    Console.WriteLine("You have " + animal_in_cell.Length + " cells\n");
                    break;
                case "5":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Wrong input\n");
                    break;
            }

            show_menu();
        }


    }

    class Animal
    {
        public bool can_be_food;
        public int animal_size; // 0-tiny 1-small(dog\cat size) 2-normal(human size) 3-big(horse size) 4-Huge(elephant\jiraffe) 
        public string animal_type;
        public string name;
        public bool is_hungry;

        public Animal()
        {
            is_hungry = true; // true-hungry false-not hungry
        }

        public void show_info()
        {
            Console.WriteLine("Animal type = " + animal_type);
            Console.WriteLine("Can be food = " + can_be_food);
            Console.WriteLine("Animal size = " + animal_size);
            Console.WriteLine("Animal name = " + name);
            Console.WriteLine("Is hungry = " + is_hungry);
            Console.WriteLine("\n");
        }

    }
    class Predator:Animal
    {
      
      public Predator() //constructor
      {
          can_be_food = false;
      
      }


    }

    class Bear : Predator
    {
        public Bear(string obj_name)
        {
            animal_size = 2;
            animal_type="Bear";
            name = obj_name;

        }
    
    }


    class Herbivores:Animal
    {

        public Herbivores() //constructor
        {
            can_be_food = true;

        }


    }



    class Rabbit : Herbivores
    {
        public Rabbit(string obj_name)
        {
            animal_size = 1;
            animal_type = "Rabbit";
            name = obj_name;

        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Menu game_menu = new Menu();
            game_menu.show_menu();
                
        }
    }
}
