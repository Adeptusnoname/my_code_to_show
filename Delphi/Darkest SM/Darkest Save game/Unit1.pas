unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,ShlObj, xmldom, XMLIntf, msxmldom, XMLDoc,ShellAPI,
  ComCtrls, Grids, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, jpeg, ExtCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    Label2: TLabel;
    Button3: TButton;
    ListBox1: TListBox;
    ListBox2: TListBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    ListBox3: TListBox;
    ListBox4: TListBox;
    ListBox5: TListBox;
    Button4: TButton;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    ListBox6: TListBox;
    ListBox7: TListBox;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function ExecAndWait(const FileName, Params: ShortString; const WinState: Word): boolean; export;
var
  StartInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
  CmdLine: ShortString;
begin
  { ???????? ??? ????? ????? ?????????, ? ??????????? ???? ???????? ? ?????? Win9x }
  CmdLine := '"' + Filename + '" ' + Params;
  FillChar(StartInfo, SizeOf(StartInfo), #0);
  with StartInfo do
  begin
    cb := SizeOf(StartInfo);
    dwFlags := STARTF_USESHOWWINDOW;
    wShowWindow := WinState;
  end;
  Result := CreateProcess(nil, PChar( String( CmdLine ) ), nil, nil, false,
                          CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil,
                          PChar(ExtractFilePath(Filename)),StartInfo,ProcInfo);
  { ??????? ?????????? ?????????? }
  if Result then
  begin
    WaitForSingleObject(ProcInfo.hProcess, INFINITE);
    { Free the Handles }
    CloseHandle(ProcInfo.hProcess);
    CloseHandle(ProcInfo.hThread);
  end;
end;


function GetMyDoc : string;
var
  SpecialDir: PItemIdList;
begin
    SetLength(result, MAX_PATH);
    SHGetSpecialFolderLocation(Form1.Handle, CSIDL_PERSONAL, SpecialDir);
    SHGetPathFromIDList(SpecialDir, PChar(Result));
    SetLength(result, lStrLen(PChar(Result)));
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  p:PAnsiChar;
  ms:TMemoryStream;
//
F: TextFile;
s,FileName,a,path: string;
savedir,words:string;
List:TStringList;
i:integer;
SR: TSearchRec; // ��������� ����������
FindRes: Integer; // ���������� ��� ������ ���������� ������begin
begin
//Application.Icon.LoadFromFile('save24.bmp');
words:='';
savedir:='';
//get path
for i:=1 to Length(GetMyDoc) do
  begin
    words:=words+GetMyDoc[i];
      if GetMyDoc[i]='\' then
        begin
          savedir:=savedir+words;
          words:='';
        end;
  end;

ListBox1.Clear;
savedir:=savedir+'Saved Games\Darkest\';
//ShowMessage(savedir);
//FindRes := FindFirst('c:\\delphi\\*.*', faAnyFile, SR);
FindRes := FindFirst(savedir+'*.*', faAnyFile, SR);
  while FindRes = 0 do
  begin
   if ((SR.Attr and faDirectory) = faDirectory) and
    ((SR.Name = '.') or (SR.Name = '..')) then
    begin
      FindRes := FindNext(SR);
      Continue;
    end;

   if ((SR.Attr and faDirectory) = faDirectory) and
    ((SR.Name[1] = 'p') or (SR.Name[2] = 'r')) then
    begin
    ListBox1.Items.Add(SR.Name);
    ListBox2.Items.Add(savedir+SR.Name);
    FindRes := FindNext(SR);
    Continue;
    end;

  FindRes := FindNext(SR);

  end;
  FindClose(SR);

   path:= ExtractFilePath(ParamStr(0));
 // showmessage(path+'save\');
  FindRes := FindFirst(path+'save\'+'*.*', faAnyFile, SR);
  while FindRes = 0 do
  begin
   if ((SR.Attr and faDirectory) = faDirectory) and
    ((SR.Name = '.') or (SR.Name = '..')) then
    begin
      FindRes := FindNext(SR);
      Continue;
    end;

   if ((SR.Attr and faDirectory) = faDirectory)  then
    begin
     //Name+date
    ListBox5.Items.Add(SR.Name);
     //NAME+date
    FindRes := FindNext(SR);
    Continue;
    end;

  FindRes := FindNext(SR);

  end;
  FindClose(SR);

  //get save name
 // Memo3.Lines.LoadFromFile(ListBox2.Items[1]+'\persist.game.json')

  For i:=0 to ListBox2.Items.Count-1 do
  begin
      CopyFile(PChar(ListBox2.Items[i]+'\persist.game.json'),(PChar(ListBox2.Items[i]+'\persisttemp.txt')),true);
  //  !!
      ms:=TMemoryStream.Create;
      ms.loadFromFile(ListBox2.Items[i]+'\persisttemp.txt');
      p:=PAnsiChar(ms.memory)+ms.size-2;
  while p^<>#0 do
      begin
    p:=p-1;
      end;
    s:=p+1;
    ListBox3.Items.Add(s);
    ListBox4.Items.Add(s);
    ms.free;
  //  !!
  end;

  end;


procedure GetAllFiles( Path: string; Lb: TListBox );
var
sRec: TSearchRec;
isFound: boolean;
begin
isFound := FindFirst( Path + '\*.*', faAnyFile, sRec ) = 0;
while isFound do
begin
if ( sRec.Name <> '.' ) and ( sRec.Name <> '..' ) then
begin
if ( sRec.Attr and faDirectory ) = faDirectory then
GetAllFiles( Path + '\' + sRec.Name, Lb );
if ( sRec.Attr)<>faDirectory then
Lb.Items.Add( Path + '\' + sRec.Name );
end;
Application.ProcessMessages;
isFound := FindNext( sRec ) = 0;
end;
FindClose( sRec );
end;
  
procedure TForm1.Button1Click(Sender: TObject);
var
  Res,i,k,j,fl,r,ind,len : Integer;
  Sd : TSaveDialog;
  lt : TSYSTEMTIME;
  str,newf,newfol,month,day,hour,minut,sec:string;
begin
  //ExtractFilePath(ParamStr(0))
  ListBox6.Items.Clear;
  GetLocalTime(lt);
  ind:= ListBox4.ItemIndex;
  try
  if lt.wmonth<10 then month:='0'+IntToStr(lt.wmonth)
  else
  month:=IntToStr(lt.wmonth);
  if lt.wDay<10 then day:='0'+IntToStr(lt.wDay)
  else
  day:=IntToStr(lt.wDay);
  if lt.wHour<10 then hour:='0'+IntToStr(lt.wHour)
  else
  hour:= IntToStr(lt.wHour);
  if lt.wMinute<10 then minut:='0'+IntToStr(lt.wMinute)
  else
  minut:=IntToStr(lt.wMinute);
  if lt.wSecond<10 then sec:='0'+IntToStr(lt.wSecond)
  else
  sec:=IntToStr(lt.wSecond);
  newf:=(ExtractFilePath(ParamStr(0))+'save\'+String(ListBox4.Items[ind])+' ' + IntToStr(lt.wYear)+'.'+month+'.'+day+'.'+hour+'.'+minut+'.'+sec);



 // ShowMessage(newf);
  except
  ShowMessage('�� ������� ������� ���������� ��� �����������');
  exit;
  end;
  CreateDir(newf);
  CreateDir(newf+'\backup');

  {
  Sd := SaveDialog1;
  if Sd.InitialDir = '' then
    Sd.InitialDir := ExtractFilePath( Application.ExeName )  ;

  if not Sd.Execute then Exit;
  if FileExists(Sd.FileName) then begin
    Res := MessageDlg(
      '���� � �������� ������ ��� ����������. ������������?'
      , mtConfirmation, [mbYes, mbNo], 0
    );
    if Res <> mrYes then Exit;
  end;

  //����� ��� ��� ������ ������ � ���� � ������ Sd.FileName.
  //...
  }
  GetAllFiles(Listbox2.Items[ind], Listbox6 );

  For k:=0 to Listbox6.Count-1 do
    begin
      For j:=0 to Length(Listbox6.Items[k]) do
        begin
          if Listbox6.Items[k][j]<>'\' then
          newfol:=newfol+Listbox6.Items[k][j]
          else
          newfol:='';
        end;
        Listbox7.Items.Add(newfol);
    end;

  For i:=0 to Listbox6.Count-1 do
    begin
     For j:=0 to Length(Listbox6.Items[i]) do
        begin
          if Listbox6.Items[i][j]<>'\' then
          newfol:=newfol+Listbox6.Items[i][j]
          else
           begin
            if newfol='backup' then
              begin
                fl:=1;
              end;
              newfol:='';
           end;
        end;
    if fl=1 then
      begin
      CopyFile(Pchar(Listbox6.Items[i]),Pchar(newf+'\backup\'+Listbox7.Items[i]),true);
      fl:=0;
      end
    else
      begin
      fl:=0;
      CopyFile(Pchar(Listbox6.Items[i]),Pchar(newf+'\'+Listbox7.Items[i]),true);
      end;

    end;
    Button3.Click;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  p:PAnsiChar;
  ms:TMemoryStream;
//
F: TextFile;
s,FileName,a,name,date,path,str: string;
savedir,words:string;
List:TStringList;
i,j,k:integer;
SR: TSearchRec; // ��������� ����������
FindRes: Integer; // ���������� ��� ������ ���������� ������begin
begin
words:='';
savedir:='';
Listbox1.Items.Clear;
Listbox2.Items.Clear;
Listbox3.Items.Clear;
Listbox4.Items.Clear;
Listbox5.Items.Clear;
Listbox6.Items.Clear;
//get path
for i:=1 to Length(GetMyDoc) do
  begin
    words:=words+GetMyDoc[i];
      if GetMyDoc[i]='\' then
        begin
          savedir:=savedir+words;
          words:='';
        end;
  end;

ListBox1.Clear;
savedir:=savedir+'Saved Games\Darkest\';
//ShowMessage(savedir);
//FindRes := FindFirst('c:\\delphi\\*.*', faAnyFile, SR);
FindRes := FindFirst(savedir+'*.*', faAnyFile, SR);
  while FindRes = 0 do
  begin
   if ((SR.Attr and faDirectory) = faDirectory) and
    ((SR.Name = '.') or (SR.Name = '..')) then
    begin
      FindRes := FindNext(SR);
      Continue;
    end;

   if ((SR.Attr and faDirectory) = faDirectory) and
    ((SR.Name[1] = 'p') or (SR.Name[2] = 'r')) then
    begin
    ListBox1.Items.Add(SR.Name);
    ListBox2.Items.Add(savedir+SR.Name);
    FindRes := FindNext(SR);
    Continue;
    end;

  FindRes := FindNext(SR);

  end;
  FindClose(SR);

  //get save name
 // Memo3.Lines.LoadFromFile(ListBox2.Items[1]+'\persist.game.json')

  For i:=0 to ListBox2.Items.Count-1 do
  begin
      CopyFile(PChar(ListBox2.Items[i]+'\persist.game.json'),(PChar(ListBox2.Items[i]+'\persisttemp.txt')),true);
  //  !!
      ms:=TMemoryStream.Create;
      ms.loadFromFile(ListBox2.Items[i]+'\persisttemp.txt');
      p:=PAnsiChar(ms.memory)+ms.size-2;
  while p^<>#0 do
      begin
    p:=p-1;
      end;
    s:=p+1;
    ListBox3.Items.Add(s);
    ListBox4.Items.Add(s);
    ms.free;
  //  !!
  end;

  // Find save
  path:= ExtractFilePath(ParamStr(0));
 // showmessage(path+'save\');
  FindRes := FindFirst(path+'save\'+'*.*', faAnyFile, SR);
  while FindRes = 0 do
  begin
   if ((SR.Attr and faDirectory) = faDirectory) and
    ((SR.Name = '.') or (SR.Name = '..')) then
    begin
      FindRes := FindNext(SR);
      Continue;
    end;

   if ((SR.Attr and faDirectory) = faDirectory)  then
    begin
     //Name+date
    ListBox5.Items.Add(SR.Name);
     //NAME+date
    FindRes := FindNext(SR);
    Continue;
    end;

  FindRes := FindNext(SR);

  end;
  FindClose(SR);

end;

procedure TForm1.Button4Click(Sender: TObject);
var
i,j,k,fl :integer;
str:string;
begin
k:=ListBox5.ItemIndex;
Listbox6.Items.Clear;
try
GetAllFiles(ExtractFilePath(ParamStr(0))+'save\'+Listbox5.Items[k], Listbox6 );
except
ShowMessage('�� ������� ���������� ��� ��������');
exit;
end;
for i:=0 to Listbox6.Items.Count-1 do
begin
DeleteFile(Listbox6.Items[i]);
end;
//ShowMessage(ExtractFilePath(ParamStr(0))+'save\'+Listbox5.Items[k]+'\backup');
RemoveDir(ExtractFilePath(ParamStr(0))+'save\'+Listbox5.Items[k]+'\backup') ;
RemoveDir(ExtractFilePath(ParamStr(0))+'save\'+Listbox5.Items[k]) ;
Button3.Click;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
i,j,sind,k,fl :integer;
strm,newfol:string;
begin
newfol:='';
sind:=ListBox4.ItemIndex;
k:=ListBox5.ItemIndex;
Listbox6.Items.Clear;
try
GetAllFiles(ExtractFilePath(ParamStr(0))+'save\'+Listbox5.Items[k], Listbox6 );
except
ShowMessage('�� ������� ���������� ��� ������');
exit;
end;
{
for i:=0 to Listbox6.Items.Count-1 do
begin

end;
}

//add
try
 For i:=0 to Listbox6.Count-1 do
    begin
     For j:=0 to Length(Listbox6.Items[i]) do
        begin
          if Listbox6.Items[i][j]<>'\' then
          newfol:=newfol+Listbox6.Items[i][j]
          else
           begin
            if newfol='backup' then
              begin
                fl:=1;
              end;
              newfol:='';
           end;
        end;
    if fl=1 then
      begin
     // showMessage(Listbox2.Items[k]+'\backup\'+newfol);
      CopyFile(Pchar(Listbox6.Items[i]),Pchar(Listbox2.Items[sind]+'\backup\'+newfol),false);
      fl:=0;
      end
    else
      begin
      fl:=0;
     // showMessage(Listbox2.Items[sind]+'\'+newfol);
      CopyFile(Pchar(Listbox6.Items[i]),Pchar(Listbox2.Items[sind]+'\'+newfol),false);
      end;

    end;
//add
except
ShowMessage('�������� ������� ���������� ������� ������ ��������');
exit;
end;

end;

end.
